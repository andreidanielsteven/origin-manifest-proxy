#!/usr/bin/env python3

# load some requests files
# without origin: print out a list of origins with number of requests
# with origin: print out the manifest

import sys, json, bz2, pprint
from OriginManifestTools import *

blacklist = [
    'content-encoding',                 # turns gzipped pages into chinese characters (browser does the unzipping before handing content to the extension)
    'access-control-allow-origin',      # fonts on chalmers.se not rendered properly
    'content-type',
    'content-length',
    'content-disposition',
]

def myopen(fn):
    fp = None
    try:
        fp = bz2.open(fn, "rt")
    except Exception as e:
        fp = open(fn)
    return fp


origin = sys.argv[1]
statefile = sys.argv[2]

OMlearner = OriginManifestLearner()
OMmerger = OriginManifestHeaderMerger()
OMgenerator = OriginManifestGenerator(OMlearner, OMmerger, cutoff=60, minsize=2, blacklist=blacklist)

OMlearner.loadState(statefile)

if origin == "":
    originlist = sorted(OMlearner.getOrigins().items(), key=lambda x: x[1], reverse=True)
    manifests = [OMgenerator.generateManifest(origin) for origin, _ in originlist]
else:
    manifests = [OMgenerator.generateManifest(origin)]

for manifest in manifests:
    pprint.pprint(manifest)


