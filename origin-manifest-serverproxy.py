from mitmproxy import http, ctx
from OriginManifestTools import OriginManifestLearner, OriginManifestGenerator, OriginManifestStore, OriginManifestApplier, OriginManifestHeaderName, OriginManifestWellKnownPathPrefix, OriginManifestContentType, OriginManifestServerHasNoVersion, OriginManifestQuote, OriginManifestHeaderMerger
from urlrouter import URLRouter, ignore
from commontools import *
import sys, json, pprint

proxyname = "serverproxy"
cutoffpercentage = 51
assumeClientSpeaksProtocol = False

manifest_directory = "{}/servermanifests".format(sys.argv[1])
savedStateFilename = "{}/serversavedstate.json".format(sys.argv[1])
statefileprefix = sys.argv[2]

blacklist = [
    'content-encoding',             # turns gzipped pages into chinese characters (browser does the unzipping before handing content to the extension)
    'access-control-allow-origin',  # fonts on chalmers.se not rendered properly
    'content-type',
    'content-length',
    'content-disposition',
]

####################################################################
### only code below this point
####################################################################

originManifestLearner = None
originManifestGenerator = None
originManifestStore = None
globalInfo = {"status": "prestart"}

@logexception(globalInfo, "response")
def response(flow: http.HTTPFlow) -> None: #{{{
    global originManifestLearner, originManifestApplier, assumeClientSpeaksProtocol
    origin = getRequestOrigin(flow)
    method = flow.request.method.upper()
    if origin not in ["http,{},80".format(x) for x in [proxyname]] and not flow.request.path.startswith(OriginManifestWellKnownPathPrefix):
        originManifestLearner.learn(method, flow.request.url, flow.response.headers)

    manifest, serverversion = originManifestStore.getManifest(getRequestOrigin(flow), 0, ignoreversion=True)

    # we don't care about the client's version, but need to check whether the origin manifest mechanism is supported
    clientversion = flow.request.headers[OriginManifestHeaderName] if OriginManifestHeaderName in flow.request.headers else None

    # Only reply with the origin manifest header if the client speaks the protocol
    if assumeClientSpeaksProtocol or clientversion != None:
        if manifest != None:
            # apply the latest origin manifest, and assume the browser will fetch the latest origin manifest if it doesn't have it already
            flow.response.headers = originManifestApplier.strip(method, origin, manifest, flow.response.headers)
            # the client supported origin manifest and there is a manifest, so send the version
            flow.response.headers[OriginManifestHeaderName] = OriginManifestQuote(serverversion)
        else:
            # There is no manifest, and we want any manifest on the clientside destroyed
            # So, we send the header with value "0"
            flow.response.headers[OriginManifestHeaderName] = OriginManifestServerHasNoVersion
#}}}
@logexception(globalInfo,"request")
def request(flow: http.HTTPFlow) -> None: #{{{
    global urlRouter

    handlercontext = {
            "flow": flow,
            "ctx": ctx,
    }

    result = urlRouter.handle(flow.request.method.upper(), flow.request.pretty_url, context=handlercontext)
    if result != None:
        if result["httpcode"] == 302:
            # special case: 302 redirect
            flow.response = http.HTTPResponse.make(result["httpcode"], result["data"], {"location": result["location"]})
        else:
            flow.response = http.HTTPResponse.make(result["httpcode"], result["data"], {"content-type": result["contenttype"]})
        return

    # del flow.response.headers[OriginManifestHeaderName] # Can't delete the header because we need it to process the response...
#}}}

def handle_get_manifest(args, response): # {{{
    global originManifestStore
    origin = getRequestOrigin(args["_context"]["flow"])

    response["contenttype"] = OriginManifestContentType

    if "version" in args:
        # if the client specifies the version, try to get that version and return it
        # if it doesn't exist, return 404
        version = args["version"]
        manifest, serverversion = originManifestStore.getManifest(origin, version, stripcomments=True)
        if manifest == None:
            response["httpcode"] = 404
            response["data"] = ""
        else:
            response["httpcode"] = 200
            response["data"] = json.dumps(manifest)
    else:
        # According to section 2.3.1, if no version is specified,
        # the server should either:
        #   redirect to the correct URL (with version) using 302
        #   or return 404 if there in no manifest
        manifest, serverversion = originManifestStore.getManifest(origin, 0, ignoreversion=True, stripcomments=True)
        if manifest == None:
            response["httpcode"] = 404
            response["data"] = ""
        else:
            response["httpcode"] = 302
            response["data"] = ""
            response["location"] = OriginManifestWellKnownPathPrefix + "/" + serverversion

    return response
#}}}
def handle_inspect(args, response): #{{{
    global originManifestLearner, originManifestGenerator, originManifestStore
    response["data"] = ""

    if "origin" in args:
        origin = args["origin"]
        rawdata = originManifestLearner.getResponseData(origin)
        manifest = originManifestGenerator.generateManifest(origin)
        storedmanifest, storedversion = originManifestStore.getManifest(origin, 0, ignoreversion=True)
        if rawdata != None:
            html = "<html><body>"

            if manifest != None:
                html += "<h2>Generated manifest:</h2>"
                html += "<b>(Based on {} responses, cutoff {} and minsize {})</b><br>".format(sum(len(urldata.keys()) for (method, urldata) in rawdata.items()), originManifestGenerator.cutoff, originManifestGenerator.minsize)
                html += "<br><pre>"
                html += pprint.pformat(manifest, width=80).replace("\n", "<br>")
                html += "</pre>"
            else:
                html += "<b>No generated manifest</b><br>"

            if storedmanifest != None:
                html += "<h2>Stored manifest (version {}):</h2>".format(storedversion)
                html += "<br><pre>"
                html += pprint.pformat(storedmanifest, width=80).replace("\n", "<br>")
                html += "</pre>"
            else:
                html += "<b>No stored manifest</b><br>"

            html += "<h2>Raw data:</h2><br><pre>"
            html += pprint.pformat(rawdata, width=80).replace("\n", "<br>")
            html += "</pre>"
            html += "</body></html>"
            response["data"] = html
    else:
        response["data"] = "<html><body><ul>"
        sortedOriginCounts = sorted(originManifestLearner.getOrigins().items(), key=lambda x: x[1], reverse=True)
        response["data"] += "\n".join(["<li><a href=\"http://{}/inspect/{}\">[{}] {}</a></li>".format(proxyname,o,c,o) for o,c in sortedOriginCounts])
        response["data"] += "</ul></body></html>"

    response["httpcode"] = 200
    return response
#}}}
def handle_dump(args, response): # {{{
    global originManifestGenerator, originManifestStore
    ctx = args["_context"]["ctx"]
    if "version" in args:
        version = args["version"]
    else:
        version = "v0"

    originManifestGenerator.generateAndWriteAll(version, originManifestStore)
    html = "<html><body>"
    html += "Manifests saved to disk, version={}".format(version)
    html += "</body></html>"

    response["data"] = html
    response["httpcode"] = 200

    ctx.log.warn("Dump successful")
    return response
#}}}
def handle_reset(args, response): # {{{
    global originManifestStore, originManifestGenerator, originManifestLearner
    ctx = args["_context"]["ctx"]
    originManifestGenerator.reset()
    originManifestLearner.reset()
    originManifestStore.reset()
    html = "<html><body>Deleted all in-memory and on-disk state</body></html>"
    response["data"] = html
    response["httpcode"] = 200

    ctx.log.warn("Reset successful")
    return response
#}}}
def handle_getstate(args, response): # {{{
    global originManifestLearner
    response["data"] = json.dumps(originManifestLearner.headerData)
    response["httpcode"] = 200
    response["contenttype"] = "application/json"
    return response
#}}}
def handle_info(args, response): # {{{
    global originManifestLearner
    response["data"] = json.dumps(globalInfo)
    response["httpcode"] = 200
    response["contenttype"] = "application/json"
    return response
#}}}
def handle_exit(args, response): # {{{
    ctx = args["_context"]["ctx"]
    ctx.log.warn("Exit on demand")
    ctx.master.should_exit.set()
    return response
#}}}
def handle_index(args, response): #{{{
    response["data"] = """
<html><body>
<h1>Server proxy</h1>
<dl>
<dt><a href="/inspect">Inspect</a></dt>
<dd>Inspect manifests and collected response headers.</dd>

<dt><a href="/dump/v0">Dump</a></dt>
<dd>Autogenerate manifests and write them to disk. This activates the serverside mechanism.</dd>

<dt><a href="/getstate">getState</a></dt>
<dd>Download all collected response headers as JSON</dd>

<dt><a href="/reset">Reset</a></dt>
<dd>Reset proxy state by removing manifests and collected response headers</dd>

<dt><a href="/info">Info</a></dt>
<dd>Return some internal proxy information</dd>

<dt><a href="/exit">Exit proxy</a></dt>
<dd>... for a clean shutdown</dd>
</dl>
</body></html>
"""
    response["httpcode"] = 200
    return response
#}}}

@logexception(globalInfo, "start")
def start(): #{{{
    global originManifestLearner, originManifestGenerator, originManifestStore, originManifestApplier, savedStateFilename, cutoffpercentage, globalInfo
    global globalInfo
    try:
        globalInfo = json.load(open("{}-globalInfo.json".format(statefileprefix)))
    except:
        pass
    originManifestLearner = OriginManifestLearner()
    originManifestLearner.loadState(savedStateFilename)
    originManifestHeaderMerger = OriginManifestHeaderMerger()
    originManifestGenerator = OriginManifestGenerator(originManifestLearner, originManifestHeaderMerger, cutoff=cutoffpercentage, blacklist=blacklist)
    originManifestStore = OriginManifestStore(manifest_directory)
    originManifestApplier = OriginManifestApplier(originManifestHeaderMerger, logfn=ctx.log.debug)

    global urlRouter
    urlRouter = URLRouter([
        # fetch manifest from well known location
        ("GET", "*", "*", "*", OriginManifestWellKnownPathPrefix, handle_get_manifest),
        ("GET", "*", "*", "*", OriginManifestWellKnownPathPrefix + "/{version:any}", handle_get_manifest),

        # proxy special routes
        ("GET", "*", proxyname, "*", '/', handle_index),
        ("GET", "*", proxyname, "*", '/inspect', handle_inspect),
        ("GET", "*", proxyname, "*", '/inspect/{origin:any}', handle_inspect),
        ("GET", "*", proxyname, "*", '/dump', handle_dump),
        ("GET", "*", proxyname, "*", '/dump/{version:any}', handle_dump),
        ("GET", "*", proxyname, "*", '/reset', handle_reset),
        ("GET", "*", proxyname, "*", '/getstate', handle_getstate),
        ("GET", "*", proxyname, "*", '/exit', handle_exit),
        ("GET", "*", proxyname, "*", '/info', handle_info),
        ("GET", "*", proxyname, "*", '/favicon.ico', ignore(200)),
        ("GET", "*", proxyname, "*", '/{any}', ignore(404)),
    ])
    globalInfo["status"] = "started"
#}}}
@logexception(globalInfo, "done")
def done(): #{{{
    global originManifestLearner, savedStateFilename, globalInfo
    originManifestLearner.writeState(savedStateFilename)
    json.dump(globalInfo, open("{}-globalInfo.json".format(statefileprefix), "w"))
#}}}


