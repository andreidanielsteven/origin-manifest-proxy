#!/usr/bin/env python3

from __future__ import unicode_literals
import json, collections, pprint, os, os.path
from urllib.parse import urlparse
from enum import Enum

OriginManifestHeaderName = "sec-origin-policy"
OriginManifestWellKnownPathPrefix = "/.well-known/origin-policy" # version will be appended
OriginManifestContentType = "application/manifest+json" # Section 3.4.2

OriginManifestSectionFallback = "fallback"
OriginManifestSectionBaseline = "baseline"
OriginManifestSectionAugmentOnly = "augmentonly"
OriginManifestSectionCorsPreflight = "cors-preflight"
OriginManifestSectionCorsPreflightWithCredentials = "unsafe-cors-preflight-with-credentials"

OriginManifestServerHasNoVersion = "0"
OriginManifestClientHasNoVersion = "1"


HACK_PSEUDO_CORS_HEADER = "x-pseudo-cors-consolidation-header"
HACK_PSEUDO_CORS_PREFLIGHT_HEADER = "x-pseudo-cors-preflight-consolidation-header"

def OriginManifestUnquote(v):
    if v[0] == '"' and v[-1] == '"':
        return v[1:-1]
    raise ValueError("'{}' is not a quoted string".format(v))

def OriginManifestQuote(v):
    return "\"{}\"".format(v)

class OriginManifestLearner(object): #{{{
    def __init__(self):
        # all origins and headernames are stored lowercase
        # structure: {
        #     "origin1": {
        #       "method1": {
        #         "url1": {
        #             "header1": "value1",
        #             "header2": "value2",
        #             ...
        #         },
        #         "url2": { ... }
        #       },  
        #     },
        #     "origin2": {...}
        # }
        self.reset()

    def reset(self):
        self.headerData = {}

    def originFromURL(self, url):
        # get the origin for a given URL in the format scheme,hostname,port all lowercase
        urlp = urlparse(url)
        scheme = urlp.scheme
        host = urlp.hostname
        port = urlp.port

        # we assume at least scheme and hostname are given...
        # but port can be omitted
        if port == None:
            port = {
                    "http": 80,
                    "https": 443
                    }[scheme]

        return "{},{},{}".format(scheme, host, port).lower()

    def loadState(self, filename):
        # load state from a previous run
        self.headerData = json.load(open(filename))

    def writeState(self, filename):
        # save state so it can be reloaded later
        json.dump(self.headerData, open(filename, "w"))

    def learn(self, method, url, headers):
        # learn the headers for a given URL
        # headers is a dict
        origin = self.originFromURL(url)
        headerslc = dict([(h.lower(),v) for (h,v) in headers.items()])

        # if the origin is new, create it
        if not origin in self.headerData:
            self.headerData[origin] = {}

        # if the method is new, create it
        if not method in self.headerData[origin]:
            self.headerData[origin][method] = {}

        # if the url is not new, only retain header+value combinations that we have seen before
        if url in self.headerData[origin][method]:
            combined = dict([(h,v) for (h,v) in self.headerData[origin][method][url].items() if (h,v) in headerslc.items()])
            self.headerData[origin][method][url] = combined
        else:
            # if the url is new, remember all header+value combinations
            self.headerData[origin][method][url] = headerslc

    def getOrigins(self):
        # get a dict with all observed origins and the amount of URLs seen in each
        return dict([(o, sum( len(m.keys()) for (x, m) in d.items())) for (o, d) in self.headerData.items()])

    def getResponseData(self, origin):
        return self.headerData[origin] if origin in self.headerData else None
#}}}
class OriginManifestGenerator(object): # {{{
    def __init__(self, learner, merger, cutoff=100, minsize=2, blacklist=[]): #{{{
        self.learner = learner
        self.merger = merger
        self.cutoff = cutoff
        self.minsize = minsize
        self.blacklist = [x.lower() for x in blacklist]
#}}}
    def generateAndWriteAll(self, version, store): #{{{
        for origin in self.learner.getOrigins().keys():
            manifest = self.generateManifest(origin)
            if manifest != None:
                store.writeManifest(origin, version, manifest)
#}}}
    def _generateFallbackManifest(self, origin, data): # {{{
        fallback = {}
        comments = {}
        urlcount = len(set(sum([list(responses.keys()) for (method, responses) in data.items()], [])))
        supportedHeaders = self.merger.getSupportedHeaders()

        # create a big list with all observed headers
        combinedheaders = []
        headercount = 0
        consideredheadercount = 0
        responsecount = 0
        blacklisted = 0
        nonapplicable = 0

        for (method, responses) in data.items():
            for (url, headers) in responses.items():
                responsecount += 1
                # only consider headers that make sense for a certain content-type and scheme
                # and that are not on the blacklist
                ctype = headers["content-type"].lower().split(";")[0] if "content-type" in headers else None
                for (h,v) in headers.items():
                    headercount += 1
                    onBlacklist = h in self.blacklist
                    # applicable means: if the header is supported by the merger, it needs to only occur for certain contenttypes and schemes
                    isApplicable = h not in supportedHeaders or self.merger.headerIsApplicable(h, ctype, origin)
                    if not onBlacklist and isApplicable:
                        combinedheaders += [(h,v)]
                        consideredheadercount += 1
                    else:
                        if onBlacklist:
                            blacklisted += 1
                        if not isApplicable:
                            nonapplicable += 1

        # create a dict with the duplicate count per header
        headercounts = collections.Counter(combinedheaders)
        # calculate how many occurrences of a header+value we need so it can appear in the origin manifest 
        cutoffcount = urlcount * self.cutoff / 100.0

        # generate the fallback manifest
        fallback = dict([k for (k,v) in headercounts.items() if v >= cutoffcount])

        # keep track of statistics
        comments = {
            "responseCount": responsecount,
            "headerCount": headercount,
            "blacklistedHeaderCount": blacklisted,
            "nonApplicableHeaderCount": nonapplicable
        }

        return fallback, comments
#}}}
    def _generateBaselineManifest(self, origin, data, augmentonly=False): # {{{
        baseline = {}
        comments = {}

        # this will be inefficient... 
        # for each supported header:
        for (supheader, ct) in self.merger.getSupportedHeaders(augmentonly=augmentonly).items():
            # get the real header name for the baseline
            (realheader, _, _, _, _, _) = self.merger.getMetaData(supheader)
            
            # how many headers resulted in this baseline?
            # how many of those headers would have voided the baseline?
            # what would the baseline have been if not voided?
            headercount = 0
            deletedcount = 0
            shouldbedeleted = False

            # go over all records and process those responses that have such a header and the corresponding content-type
            for method,methodreq in data.items():
                for url,req in methodreq.items():
                    # depending on whether this is a CORS preflight response or not, we need to set a different pseudo header
                    if method == "OPTIONS":
                        pseudoCORSHeader = HACK_PSEUDO_CORS_PREFLIGHT_HEADER
                    else:
                        pseudoCORSHeader = HACK_PSEUDO_CORS_HEADER

                    # before anything, we need to consolidate all CORS headers into a pseudo header for the merger
                    # but only if the pseudo header is being handled...
                    assert(pseudoCORSHeader not in req and pseudoCORSHeader.lower() not in req)
                    if pseudoCORSHeader.lower() == supheader.lower():
                        pseudoval = self.merger.generatePseudoCORSHeader(req)
                        if pseudoval != "{}":
                            req[pseudoCORSHeader.lower()] = pseudoval

                    reqct = req["content-type"].lower().split(";")[0] if "content-type" in req else None
                    if supheader in req and self.merger.headerIsApplicable(supheader, reqct, origin):
                        # get the server-provided header
                        header = req[supheader]
                        headercount += 1

                        # if we didn't learn anything yet, just merge the header with itself for cleanup
                        if realheader in baseline:
                            baselineheader = (realheader, baseline[realheader])
                        else:
                            baselineheader = (supheader, header)

                        # merge the headers...
                        res = self.merger.weaker(baselineheader , (supheader, header))

                        if res["operation"] in [ self.merger.Ops.OP_USE_AS_MANIFEST_VALUE ]:
                            baseline[realheader] = res["value"]
                        elif res["operation"] in [ self.merger.Ops.OP_DELETE_MANIFEST_VALUE ]:
                            deletedcount += 1
                            shouldbedeleted = True
                        elif res["operation"] in [ self.merger.Ops.OP_SET_EMPTY_MANIFEST_VALUE ]:
                            baseline[realheader] = ""
                        elif res["operation"] in [ self.merger.Ops.OP_NOOP ]:
                            pass
                        else:
                            raise Exception("Unexpected operation in return value {}".format(res))

                    # remove pseudo CORS header if it exists
                    if pseudoCORSHeader.lower() in req:
                        del req[pseudoCORSHeader.lower()]

            if headercount > 0:
                comments[realheader] = {}
                comments[realheader]["mergedHeaderCount"] = headercount
                comments[realheader]["deletedHeaderCount"] = deletedcount
                if shouldbedeleted:
                    if realheader in baseline:
                        hv = baseline[realheader]
                        del baseline[realheader] # we need to remove this header from the baseline, as agreed by the merger function conventions about return values
                    else:
                        hv = ""
                    comments[realheader]["msg"] = "Result of {} merged headers. Without {} headers which merged into an error, the result would be: \"{}\"".format(headercount, deletedcount, hv)
                    comments[realheader]["alternativeValue"] = hv
                else:
                    comments[realheader]["msg"] = "Result of {} merged headers".format(headercount)

        return baseline, comments
#}}}
    def _generateCorsPreflightManifest(self, origin, data, useCredentials=False): # {{{
        manifest = []
        comments = {}

        def getHeaderValue(headers, name, defval): #{{{
            lcdict = dict([(x.lower(), y) for (x,y) in headers.items()])
            lcname = name.lower()
            return lcdict[lcname] if lcname in lcdict else defval
        #}}}
        def simplifyWhitelist(l): #{{{
            if "*" in l:
                return ["*"]
            return list(set(l))
        #}}}
        def simplifyRules(rules): #{{{
            def _innerGroupColumnX(r, x): #{{{
                # first, convert the rules list into a makeshift dict
                temp = []
                for rec in r:
                    key = list(rec)
                    field = key.pop(x)
                    temp += [(key, field)]

                # build a makeshift dict by extracting a key (everything except element x)
                # and a field (element x). All the fields for the matching key are combined
                # and simplified.
                def _remember(l, rec): #{{{
                    reck, recv = rec
                    added = False
                    for i in range(len(l)):
                        (k,v) = l[i]
                        if k == reck: 
                            l[i] = (k, tuple(simplifyWhitelist(v + recv)))
                            added = True
                            break
                    if not added:
                        l += [rec]
                #}}}
                newtemp = []
                for key, field in temp:
                    _remember(newtemp, (key, field))

                # build a list again
                out = []
                for (key, field) in newtemp:
                    rec = list(key)
                    rec.insert(x, field)
                    rec = tuple(rec)
                    out += [rec]
                return out
            #}}}

            # more of a safety check than a trivial case
            if len(rules) == 0:
                return rules

            # convert everything into tuples, since we don't want hashing errors later on
            rules = [tuple([tuple([x for x in col]) for col in  rule]) for rule in rules]
            columncount = len(rules[0])

            while True:
                # find the column by which to group so that the most rules are eliminated
                compacted_by_col = [_innerGroupColumnX(rules, i) for i in range(columncount)]
                lengths = [len(x) for x in compacted_by_col]
                minlength = min(lengths)
                if minlength != len(rules):
                    col = lengths.index(minlength)
                    rules = compacted_by_col[col]
                else:
                    # if nothing has any effect, we reached the end
                    break

            # return a list of tuples of lists
            rules = [tuple([list([x for x in col]) for col in  rule]) for rule in rules]
            return rules 
        #}}}

        optionsresponses = data["OPTIONS"] if "OPTIONS" in data else {}

        for url, response in optionsresponses.items():
            # get all the data
            response_origins_str = getHeaderValue(response, "access-control-allow-origin", "*")
            response_headers_str = getHeaderValue(response, "access-control-allow-headers", "*")
            response_methods_str = getHeaderValue(response, "access-control-allow-methods", "*")
            response_creds = getHeaderValue(response, "access-control-allow-credentials", None) == "true"

            response_origins = [x.strip() for x in response_origins_str.split(",")]
            response_headers = [x.strip() for x in response_headers_str.split(",")]
            response_methods = [x.strip() for x in response_methods_str.split(",")]

            # only consider this response if the usage of credentials matches what we are looking for
            if response_creds == useCredentials:
                rec = (response_origins, response_methods, response_headers)
                assert(len(response_origins) > 0)
                assert(len(response_methods) > 0)
                assert(len(response_headers) > 0)
                manifest += [rec]

        # simplify (combine) the list of rules
        fullcount = len(manifest)
        if fullcount > 0:
            #comments["PreflightCount"] = "Observed {} CORS preflights".format(fullcount)
            comments["PreflightCount"] = fullcount
        manifest = simplifyRules(manifest)
        finalcount = len(manifest)

        if len(manifest) > 1:
            matchall = tuple(["*"] for _ in range(len(list(manifest[0]))))
            if matchall in manifest:
                manifest = [matchall]
                #comments["Matchall"] = "Simplified {} rules into a single match-all rule".format(finalcount)
                comments["Matchall"] = finalcount

        manifestlist = [{"origins": ", ".join(o), "methods": ", ".join(m), "headers": ", ".join(h)} for (o, m, h) in manifest]

        if fullcount > 0 and finalcount != fullcount:
            #comments["Simplification"] = "Simplified {} rules into {} combined rules".format(fullcount, finalcount)
            comments["SimplificationBefore"] = fullcount
            comments["SimplificationAfter"] = finalcount

        return manifestlist, comments
#}}}
    def generateManifest(self, origin): #{{{
        # if there is no data, don't generate a policy
        data = self.learner.getResponseData(origin)
        if data == None:
            return None

        # if there is not enough data, don't generate a policy
        urlcount = len(set(sum([list(methodreq.keys()) for (method, methodreq) in data.items()], [])))
        if urlcount < self.minsize:
            return None

        # policies can be generated independently
        fallback, fallback_comments = self._generateFallbackManifest(origin, data)
        baseline, baseline_comments = self._generateBaselineManifest(origin, data, augmentonly=False)
        augonly, augonly_comments   = self._generateBaselineManifest(origin, data, augmentonly=True)
        preflight, preflight_comments   = self._generateCorsPreflightManifest(origin, data, useCredentials=False)
        preflightcred, preflightcred_comments   = self._generateCorsPreflightManifest(origin, data, useCredentials=True)

        return {
            OriginManifestSectionFallback: fallback,
            OriginManifestSectionFallback + "_comments": fallback_comments,
            OriginManifestSectionBaseline: baseline,
            OriginManifestSectionBaseline + "_comments": baseline_comments,
            OriginManifestSectionAugmentOnly: augonly,
            OriginManifestSectionAugmentOnly + "_comments": augonly_comments,
            OriginManifestSectionCorsPreflight: preflight,
            OriginManifestSectionCorsPreflight + "_comments": preflight_comments,
            OriginManifestSectionCorsPreflightWithCredentials: preflightcred,
            OriginManifestSectionCorsPreflightWithCredentials + "_comments": preflightcred_comments,
        }
#}}}
    def reset(self): #{{{
        self.learner.reset()
#}}}
#}}}
class OriginManifestApplier(object): # {{{
    """
    Apply a manifest to a set of headers
    """
    def __init__(self, merger, logfn=None):
        self.merger = merger
        self.logfn = logfn
        pass

    def log(self, msg):
        if self.logfn != None:
            return self.logfn(msg)

    def apply(self, method, origin, manifest, headers): # {{{
        """Applies a manifest to HTTP response headers, on the client side"""
        # First, apply the fallback: all headers that were not specified by the remote party, are filled in with the value from the manifest file
        fallbackitems = manifest[OriginManifestSectionFallback].items() if OriginManifestSectionFallback in manifest else []
        for (h, v) in fallbackitems:
            if h not in headers:
                self.log("Response has no '{}' header. Setting it to manifest-value of '{}'".format(h, v))
                headers[h] = v

        contenttype = headers["content-type"].lower().split(";")[0] if "content-type" in headers else None

        # depending on whether this is a CORS preflight response or not, we need to set a different pseudo header
        if method == "OPTIONS":
            pseudoCORSHeader = HACK_PSEUDO_CORS_PREFLIGHT_HEADER
        else:
            pseudoCORSHeader = HACK_PSEUDO_CORS_HEADER

        # generate the pseudo CORS header so it can be merged with baseline
        assert(pseudoCORSHeader not in headers and pseudoCORSHeader.lower() not in headers)
        pseudoval = self.merger.generatePseudoCORSHeader(headers)
        if pseudoval != "{}":
            headers[pseudoCORSHeader.lower()] = pseudoval

        # Next, apply the baseline: strengthen every header in the response with the baseline in the manifest
        # For some headers, we have to set multiple values by using headers.add() instead of setting a dict value

        # Finally, apply the AugmentOnly headers, such as Set-Cookie. These headers will only be strengthened
        # if they were sent by the remote server.
        
        # Since both these operations are very similar, we define an inner function to deal with it

        def _innerBaselineApply(section, augmentonly):
            baselineitems = manifest[section].items() if section in manifest else []
            for (baselineheader, baselinevalue) in baselineitems:
                # if this header is not applicable to this origin or content-type,
                # then ignore the setting
                if not self.merger.headerIsApplicable(baselineheader, contenttype, origin):
                    continue

                # h is the canonical form of the header, now we look up all the aliases
                aliases = [x.lower() for x in self.merger.getAliases(baselineheader)]

                # for each header in the response that is one of the aliases,
                # merge that header with the baseline and set it in the response
                # Exception: if it's CSP, we just set the header twice and let
                #     the client compute the "stricter" operation
                for ah in aliases:
                    # if augmentonly is True, then only merge the baseline value if the header already exists (e.g. Set-Cookie)
                    # if augmentonly is False, then either merge the baseline, or set it if empty
                    alwaysapply = not augmentonly

                    if ah in headers or alwaysapply:
                        # use the value from the response if available, otherwise, assume the value set in the baseline
                        # (We can't use the empty string as the original value here, since that will break things. Instead,
                        # assume that the baseline has a sane value set. In that case, we merge the baseline value with itself)
                        origvalue = headers[ah] if ah in headers else baselinevalue
                        if baselineheader.lower() == "content-security-policy":
                                headers.add(ah, baselinevalue)
                        else:
                            res = self.merger.stricter((baselineheader,baselinevalue), (ah,origvalue), augmentonly=augmentonly)
                            mergedval = res["value"]

                            if res["operation"] in [ self.merger.Ops.OP_USE_IN_RESPONSE_HEADER ]:
                                headers[ah] = mergedval
                            elif res["operation"] in [ self.merger.Ops.OP_DELETE_RESPONSE_HEADER ]:
                                if ah in headers:
                                    del headers[ah]
                            elif res["operation"] in [ self.merger.Ops.OP_SET_EMPTY_RESPONSE_HEADER ]:
                                headers[ah] = ""
                            elif res["operation"] in [ self.merger.Ops.OP_NOOP, self.merger.Ops.OP_SEND_CORS_PREFLIGHT, self.merger.Ops.OP_FALLBACK_TO_CORS_RESPONSE ]:
                                pass
                            else:
                                raise Exception("Unexpected operation in return value {}".format(res))

        _innerBaselineApply(OriginManifestSectionBaseline, False)
        _innerBaselineApply(OriginManifestSectionAugmentOnly, True)

        # expand the pseudo CORS header (if it exists) into individual headers.
        # all CORS headers were encoded in the pseudo header. The application of the baseline should have made them more strict
        # the result is a stricter pseudo header, with possible additional headers, but not less.
        # so we can just safely overwrite the current headers and not need to delete any from the response
        # Afterwards, remove pseudo CORS header
        if pseudoCORSHeader.lower() in headers:
            pseudoval = json.loads(headers[pseudoCORSHeader.lower()])
            for h,v in pseudoval.items():
                headers[h.lower()] = v
            del headers[pseudoCORSHeader.lower()]

        return headers
    #}}}
    def strip(self, method, origin, manifest, headers): #{{{
        """Applies a manifest to HTTP response headers, on the server side"""
        fallbackitems = manifest[OriginManifestSectionFallback].items() if OriginManifestSectionFallback in manifest else []
        for (h, v) in fallbackitems:
            if h in headers and headers[h] == v:
                self.log("Response has a '{}' header with manifest-value of '{}'. Removing it.".format(h, v))
                del headers[h]
        return headers
    #}}}
    def getCORSBypass(self, manifest, reqheaders): #{{{
        """
        given a CORS request, check if the manifest whitelists it. If it does,
        respond with the reply to send back to the client
        """

        # There is a difference between the OPTIONS request method/headers ("OPTIONS" and reqheaders)
        # and the methods/headers it inquires about. (method, headers)

        DoesNotBypass = None

        simplemethods = [ "GET", "HEAD", "POST" ]
        simpleheaders = [ "accept", "accept-language", "content-language", "content-type" ]
        simplecontenttypes = [ "application/x-www-form-urlencoded", "multipart/form-data", "text/plain" ]
        simpleresponseheaders = [ "cache-control", "content-language", "content-type", "expires", "last-modified", "pragma" ]

        def getHeaderValue(headers, name, defval): #{{{
            lcdict = dict([(x.lower(), y) for (x,y) in headers.items()])
            lcname = name.lower()
            return lcdict[lcname] if lcname in lcdict else defval
        #}}}
        def isSimpleMethod(m): #{{{
            return m in simplemethods
        #}}}
        def isSimpleHeader(h): #{{{
            # We don't check Content-Type values here...
            return h.lower() in [x.lower() for x in simpleheaders]
        #}}}
        def isSimpleReponseHeader(h, v): #{{{
            return h.lower() in [x.lower() for x in simpleresponseheaders]
        #}}}
        def usesCredentials(headers): #{{{
            # According to CORS spec, "user credentials" means that the request contains cookies,
            # HTTP authentication or client-side SSL certificates.
            # TODO: we can't detect SSL certificates... :(
            lcheaders = [x.lower() for x in headers.keys()]
            return "cookie" in headers or "authorization" in headers
        #}}}

        def matchesSingleManifestEntry(rules, origin, method, headers): #{{{
            # get the origins, methods and headers options from the manifest section
            rule_origins = rules["origins"] if "origins" in rules else []
            rule_methods = rules["methods"] if "methods" in rules else []
            rule_headers = rules["headers"] if "headers" in rules else []

            if origin == None:
                return False

            # 3.4.4 Step 5.2:
            if useCredentials and ("*" in rule_origins or "*" in rule_methods or "*" in rule_headers):
                return False

            # 3.4.4 Step 5.3:
            if not ("*" in rule_origins) and not (origin in rule_origins):
                return False

            # 3.4.4 Step 5.4:
            if not isSimpleMethod(method) and not ("*" in rule_methods) and not (method in rule_methods):
                return False

            # 3.4.4 Step 5.5:
            # all headers need to be either safelisted, or in rule_headers
            if not all([isSimpleHeader(x) or (x.lower() in rule_headers) for x in headers]) and not ("*" in rule_headers):
                return False

            return True
        #}}}


        # get the origin, method and headers from reqheaders
        origin = getHeaderValue(reqheaders, "origin", None)
        method = getHeaderValue(reqheaders, "access-control-request-method", None)
        headers_str = getHeaderValue(reqheaders, "access-control-request-headers", "")
        headers = [x.strip() for x in headers_str.split(",") if x.strip() != ""]

        # decide on which ruleset to use, based on whether the preflight uses credentials
        useCredentials = usesCredentials(reqheaders)

        if useCredentials:
            section = OriginManifestSectionCorsPreflightWithCredentials
        else:
            section = OriginManifestSectionCorsPreflight
        corsrules = manifest[section] if section in manifest else []

        # check whether any of the rules match. If none match, then we don't bypass CORS preflight
        if not any([matchesSingleManifestEntry(entry, origin, method, headers) for entry in corsrules]):
            return DoesNotBypass
        
        # formulate the response according to 
        # https://www.w3.org/TR/cors/#resource-preflight-requests step 7-
        Bypass = {}
        Bypass["access-control-allow-origin"] = origin
        Bypass["access-control-allow-methods"] = method
        if len(headers) > 0:
            Bypass["access-control-allow-headers"] = ", ".join(headers)
        # Bypass["access-control-max-age"] = ... # optional
        if useCredentials:
            Bypass["access-control-allow-credentials"] = "true"

        return Bypass
    #}}}
#}}}
class OriginManifestStore(object): #{{{
    def __init__(self, storedir):
        self.storedir = storedir
        self.pathtemplate = "{}/{{}}.json".format(storedir)

    def writeManifest(self, origin, version, manifest):
        manifest["version"] = version
        with open(self.pathtemplate.format(origin), "w") as fp:
            json.dump(manifest, fp)

    def getManifest(self, origin, version, ignoreversion=False, stripcomments=False):
        fn = os.path.abspath(self.pathtemplate.format(origin))
        dn = os.path.abspath(self.storedir)

        if not fn.startswith(dn):
            # directory traversal...
            return None, None

        try:
            with open(fn) as fp:
                manifest = json.load(fp)
                if manifest["version"] == version or ignoreversion:
                    storedversion = manifest["version"]
                    del manifest["version"] # no need to leak internal data

                    if stripcomments:
                        manifest = dict([(k,v) for (k,v) in manifest.items() if not k.endswith("_comments")])

                    return manifest, storedversion
                return None, None
        except:
            pass

        return None, None

    def deleteManifest(self, origin):
        try:
            os.remove(self.pathtemplate.format(origin))
        except FileNotFoundError:
            pass

    def getOrigins(self):
        out = []
        for f in os.listdir(self.storedir):
            origin, ext = os.path.splitext(f)
            if ext == ".json":
                out += [origin]
        return out

    def reset(self):
        # remove all manifests
        for f in os.listdir(self.storedir):
            if f != ".keepme":
                os.remove(os.path.join(self.storedir, f))
#}}}
class OriginManifestHeaderMerger(object): #{{{
    CTALL = ("*", )
    CTHTML = ("text/html",)

    SCHEMEALL = ("*", )
    SCHEMESSL = ("https",)

    HACK_CORS_HEADERS = [x.lower() for x in [
        "access-control-allow-origin",
        "access-control-allow-credentials",
        "access-control-expose-headers",
        "access-control-max-age",
        "access-control-allow-methods",
        "access-control-allow-headers",
    ]]

    def __init__(self):
        from http_header_mergers.hpkp_merger import mergeToWeaker as hpkp_weaker, applyToHeader as hpkp_stricter
        from http_header_mergers.hsts_merger import mergeToWeaker as hsts_weaker, applyToHeader as hsts_stricter
        from http_header_mergers.contentsecuritypolicy_merger import mergeToWeaker as csp_weaker, applyToHeader as csp_stricter
        from http_header_mergers.x_content_type_options_merger import mergeToWeaker as cto_weaker, applyToHeader as cto_stricter
        from http_header_mergers.x_frame_options_merger import mergeToWeaker as xfo_weaker, applyToHeader as xfo_stricter
        from http_header_mergers.x_xss_protection_merger import mergeToWeaker as xssp_weaker, applyToHeader as xssp_stricter
        from http_header_mergers.timing_allow_origin_merger import mergeToWeaker as tao_weaker, applyToHeader as tao_stricter
        from http_header_mergers.set_cookie_merger import mergeToWeaker as cookie_weaker, applyToHeader as cookie_stricter
        from http_header_mergers.cors_merger import mergeToWeaker as cors_weaker, applyToHeader as cors_stricter
        from http_header_mergers.cors_preflight_merger import mergeToWeaker as corspf_weaker, applyToHeader as corspf_stricter

        from http_header_mergers.merge_operation import Ops
        self.Ops = Ops

        self._fnmap = {
            # (header name, content-type, scheme, augmentonly, weakerfn, stricterfn): [ list of aliases ]
            ("public-key-pins",                 self.CTALL, self.SCHEMESSL, False, hpkp_weaker, hpkp_stricter): ["public-key-pins"],
            ("strict-transport-security",       self.CTALL, self.SCHEMESSL, False, hsts_weaker, hsts_stricter): ["strict-transport-security"],
            ("x-content-type-options",          self.CTALL, self.SCHEMEALL, False, cto_weaker, cto_stricter): ["x-content-type-options"],
            ("timing-allow-origin",             self.CTALL, self.SCHEMEALL, False, tao_weaker, tao_stricter): ["timing-allow-origin"],
            ("x-frame-options",                 self.CTHTML, self.SCHEMEALL, False, xfo_weaker, xfo_stricter): ["x-frame-options"],
            ("x-xss-protection",                self.CTHTML, self.SCHEMEALL, False, xssp_weaker, xssp_stricter): ["x-xss-protection"],
            ("content-security-policy",         self.CTHTML, self.SCHEMEALL, False, csp_weaker, csp_stricter): ["content-security-policy", "x-content-security-policy"],
            (HACK_PSEUDO_CORS_HEADER,           self.CTALL, self.SCHEMEALL, False, cors_weaker, cors_stricter): [HACK_PSEUDO_CORS_HEADER],
            (HACK_PSEUDO_CORS_PREFLIGHT_HEADER, self.CTALL, self.SCHEMEALL, False, corspf_weaker, corspf_stricter): [HACK_PSEUDO_CORS_PREFLIGHT_HEADER],
            ("set-cookie",                      self.CTALL, self.SCHEMEALL, True, cookie_weaker, cookie_stricter): ["set-cookie"],
            # ("content-security-policy-report-only", self.CTHTML, self.SCHEMEALL, False, csp_weaker, csp_stricter): ["content-security-policy-report-only", "x-content-security-policy-report-only"],
            # ("public-key-pins-report-only", self.CTALL, self.SCHEMEALL, False, hpkp_weaker, hpkp_stricter): ["public-key-pins-report-only"],
        }

        headermapll = [[(x.lower(), meta) for x in hl] for (meta, hl) in self._fnmap.items()]
        self.headermap = dict([item for sublist in headermapll for item in sublist])

    def getSupportedHeaders(self, augmentonly=False):
        return dict([ (x.lower(), list(self.headermap[x])[1]) for x in self.headermap.keys() if self.headermap[x][3] == augmentonly])

    def getAliases(self, header):
        res = [aliases for (hrec, aliases) in self._fnmap.items() if hrec[0].lower() == header.lower()]
        assert(len(res) <= 1)
        if len(res) > 0:
            return res[0]
        else:
            return []

    # is the header applicable in a certain context?
    # For instance, CSP only works on content-type text/html, HSTS and HPKP when scheme is https
    def headerIsApplicable(self, header, contenttype, origin):
        (_, hct, hsch, _, _, _) = self.getMetaData(header)
        scheme = origin.split(",")[0]
        contenttype = contenttype.lower() if contenttype != None else None

        # check that the content-type allows the header
        if not (hct == self.CTALL or contenttype in hct):
            return False

        # check that the origin allows the header
        if not (hsch == self.SCHEMEALL or scheme.lower() in hsch):
            return False

        return True

    def getMetaData(self, header):
        if header.lower() in self.headermap:
            return self.headermap[header.lower()]
        return (None, None, None, None, None, None)

    def _doMergeCheck(self, hv1, hv2):
        (header1, _) = hv1
        (header2, _) = hv2
        (h1, ct, sch, aug, fnw1, fns1) = self.getMetaData(header1)
        (h2, _, _, _, _, _) = self.getMetaData(header2)

        if h1 == None:
            raise ValueError("Unknown header {}".format(header1))
        if h2 == None:
            raise ValueError("Unknown header {}".format(header2))
        if h1 != h2:
            raise ValueError("Header mismatch {} <> {}".format(h1, h2))

        return (h1, ct, sch, aug, fnw1, fns1)

    # This method is an interface to the merger functions,
    # handles all weaker and stricter merging, and any errors.
    # The output of this method indicates what needs to happen
    # to either the manifest value or the response header
    # Return value:
    # {
    #     "operation": <operation>
    #     "header": <resulting header>
    #     "value": <resulting value>
    # }

    def handleOperation(self, policy_hv, new_hv, operation, augmentonly=False):
        (header1, value1) = policy_hv
        (header2, value2) = new_hv
        (h, _, _, _, fnw, fns) = self._doMergeCheck(policy_hv, new_hv)

        if operation == "weaker":
            fn = fnw
        elif operation == "stricter":
            fn = fns
        else:
            raise Exception("Unknown operation {}".format(operation))

        res = {}

        try:
            res = fn(value1, value2).toDict()
        except Exception as e:
            raise ValueError("Merger threw exception with values {} and {}: {}".format(policy_hv, new_hv, e))

        if "value" not in res:
            raise ValueError("Merger returned without value, with values {} and {}".format(policy_hv, new_hv))

        if "operation" not in res:
            raise ValueError("Merger returned without operation, with values {} and {}".format(policy_hv, new_hv))

        if res["operation"] not in [x for x in self.Ops]:
            raise ValueError("Merger returned with invalid operation {}, with values {} and {}".format(res["operation"], policy_hv, new_hv))

        res["header"] = h
        return res

    def weaker(self, policy_hv, new_hv, augmentonly=False):
        return self.handleOperation(policy_hv, new_hv, "weaker", augmentonly=augmentonly)

    def stricter(self, policy_hv, new_hv, augmentonly=False):
        return self.handleOperation(policy_hv, new_hv, "stricter", augmentonly=augmentonly)


    def generatePseudoCORSHeader(self, headers): #{{{
        # To be able to use the CORS merger, all CORS headers must be combined into a pseudo header
        outdict = dict([(x.lower(),headers[x]) for x in self.HACK_CORS_HEADERS if x.lower() in headers])
        out = json.dumps(outdict)
        return out
    #}}}
#}}}



