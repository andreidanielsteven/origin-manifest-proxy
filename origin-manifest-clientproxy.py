from mitmproxy import http, ctx
from OriginManifestTools import OriginManifestStore, OriginManifestApplier, OriginManifestHeaderName, OriginManifestWellKnownPathPrefix, OriginManifestClientHasNoVersion, OriginManifestServerHasNoVersion, OriginManifestQuote, OriginManifestUnquote, OriginManifestHeaderMerger
from urlrouter import URLRouter, ignore
from commontools import *
from urllib.parse import urlparse

import sys, pprint, json, requests, urllib3

proxyname = "clientproxy"

manifest_directory = "{}/clientmanifests".format(sys.argv[1])
statefileprefix = sys.argv[2]

####################################################################
### only code below this point
####################################################################

originManifestStore = None
originManifestApplier = None
globalInfo = {"status": "prestart"}

def downloadManifestForURL(url, serverversion): # {{{
    urlparts = urlparse(url)
    manifesturl = "{}://{}{}/{}".format(urlparts.scheme, urlparts.netloc, OriginManifestWellKnownPathPrefix, serverversion)
    manifest = None

    try:
        # fetch file through the proxy, and ignore SSL stuff
        proxies = {
            "http": ctx.master.options.upstream_server,
            "https": ctx.master.options.upstream_server,
        }
        result = requests.get(manifesturl, verify=not ctx.master.options.ssl_insecure, proxies=proxies)
        manifest = result.json()
    except Exception as e:
        ctx.log.error("downloadManifestForURL() resulted in error for manifesturl {}: {}".format(manifesturl, e))

    if manifest == None:
        ctx.log.warn("Failed to fetch manifest version {} from {}".format(serverversion, manifesturl))
    else:
        ctx.log.debug("Successfully fetched manifest version {} from {}".format(serverversion, manifesturl))
    return manifest
#}}}
@logexception(globalInfo, "response")
def response(flow: http.HTTPFlow) -> None: # {{{
    global originManifestStore, originManifestApplier

    # get manifest and version from local store
    origin = getRequestOrigin(flow)
    method = flow.request.method.upper()
    manifest, clientversion = originManifestStore.getManifest(origin, 0, ignoreversion=True)

    # check if the server speaks the origin manifest mechanism
    serverversion = flow.response.headers[OriginManifestHeaderName] if OriginManifestHeaderName in flow.response.headers else None

    # Only reply with the origin manifest header if the server speaks the protocol
    if serverversion != None:
        # don't show the header to the client
        del flow.response.headers[OriginManifestHeaderName]

        # if the server wants us to destroy the local manifest, do it
        if serverversion == OriginManifestServerHasNoVersion:
            originManifestStore.deleteManifest(origin)
            return

        # decode the server version
        serverversion = OriginManifestUnquote(serverversion)

        # if the local version is different from the server version, update the local version
        if serverversion != clientversion:
            # download manifest and store it
            manifest = downloadManifestForURL(flow.request.pretty_url, serverversion)
            if manifest == None:
                # if manifest is None, that means either there is no manifest, or there was an error in fetching it
                # either way, there is a problem because the latest manifest can not be served to the client
                # and we don't know how to modify the response either.
                # The safest thing to do, is just abort the entire caboodle and return a HTTP error code 500
                # to the client to indicate that the resource could not be retrieved.
                flow.response = http.HTTPResponse.make(500, "", {"x-reason": "Response annihilated because the origin manifest could not be processed."})
                return

            originManifestStore.writeManifest(origin, serverversion, manifest)

            manifest, clientversion = originManifestStore.getManifest(origin, serverversion, ignoreversion=False)
            assert(clientversion == serverversion)

        if manifest != None:
            flow.response.headers = originManifestApplier.apply(method, origin, manifest, flow.response.headers)
#}}}
@logexception(globalInfo, "request")
def request(flow: http.HTTPFlow) -> None: # {{{
    global urlRouter, originManifestStore, originManifestApplier

    origin = getRequestOrigin(flow)
    manifest, clientversion = originManifestStore.getManifest(origin, 0, ignoreversion=True)

    handlercontext = {
            "flow": flow,
            "ctx": ctx,
    }


    result = urlRouter.handle(flow.request.method.upper(), flow.request.pretty_url, context=handlercontext)
    if result != None:
        if "preflight" in result:
            # special case: CORS preflight
            ctx.log.debug("CORS Bypass requested for {}".format(flow.request.pretty_url))
            processedheaders = originManifestApplier.apply("OPTIONS", origin, manifest, result["preflightheaders"])
            flow.response = http.HTTPResponse.make(200, "", processedheaders)
        elif result["httpcode"] == 302:
            # special case: 302 redirect
            flow.response = http.HTTPResponse.make(result["httpcode"], result["data"], {"location": result["location"]})
        else:
            flow.response = http.HTTPResponse.make(result["httpcode"], result["data"], {"content-type": result["contenttype"]})
        return

    # add request header
    flow.request.headers[OriginManifestHeaderName] = OriginManifestQuote(clientversion) if clientversion != None else OriginManifestClientHasNoVersion
#}}}

def handle_inspect(args, response): # {{{
    global originManifestStore
    response["data"] = ""

    if "origin" in args:
        origin = args["origin"]
        manifest, version = originManifestStore.getManifest(origin, 0, ignoreversion=True)
        html = "<html><body>"
        if manifest != None:
            html += "<h2>Stored manifest for {} (version {}):</h2><br><pre>".format(origin, version)
            html += pprint.pformat(manifest, width=80).replace("\n", "<br>")
            html += "</pre>"
        else:
            html += "<h2>No stored manifest</h2>"
        html += "</body></html>"
        response["data"] = html
    else:
        response["data"] = "<html><body><ul>"
        origins = sorted(originManifestStore.getOrigins())
        originVersions = [(o, list(originManifestStore.getManifest(o, 0, ignoreversion=True))[1]) for o in origins]
        response["data"] += "\n".join(["<li><a href=\"http://{}/inspect/{}\">[{}] {}</a></li>".format(proxyname,o,v,o) for (o,v) in originVersions])
        response["data"] += "</ul></body></html>".format(proxyname)

    response["httpcode"] = 200
    return response
#}}}
def handle_reset(args, response): # {{{
    global originManifestStore
    ctx = args["_context"]["ctx"]
    originManifestStore.reset()
    html = "<html><body>Deleted all in-memory and on-disk state</body></html>"
    response["data"] = html
    response["httpcode"] = 200

    ctx.log.warn("Reset successful")
    return response
#}}}
def handle_exit(args, response): # {{{
    ctx = args["_context"]["ctx"]
    ctx.log.warn("Exit on demand")
    ctx.master.should_exit.set()
    return response
#}}}
def handle_getstate(args, response): # {{{
    global originManifestLearner
    response["data"] = json.dumps({})
    response["httpcode"] = 200
    response["contenttype"] = "application/json"
    return response
#}}}
def handle_info(args, response): # {{{
    global originManifestLearner
    response["data"] = json.dumps(globalInfo)
    response["httpcode"] = 200
    response["contenttype"] = "application/json"
    return response
#}}}
def handle_index(args, response): #{{{
    response["data"] = """
<html><body>
<h1>Client proxy</h1>
<dl>
<dt><a href="/inspect">Inspect</a></dt>
<dd>Inspect manifests and collected response headers.</dd>

<dt><a href="/reset">Reset</a></dt>
<dd>Reset proxy state by removing manifests and collected response headers</dd>

<dt><a href="/info">Info</a></dt>
<dd>Return some internal proxy information</dd>

<dt><a href="/exit">Exit proxy</a></dt>
<dd>... for a clean shutdown</dd>
</dl>
</body></html>
"""
    response["httpcode"] = 200
    return response
#}}}

def cors_preflight(args, response): # {{{
    global originManifestStore, originManifestApplier
    flow = args["_context"]["flow"]
    origin = getRequestOrigin(flow)

    # Get the manifest of the resource URL, if any
    manifest, version = originManifestStore.getManifest(origin, 0, ignoreversion=True)
    reqheaders = flow.request.headers

    # If there is no manifest (yet), don't touch the request
    if manifest == None:
        return None

    # Check if we can apply a CORS bypass
    Bypass = originManifestApplier.getCORSBypass(manifest, reqheaders)

    if Bypass != None:
        # If we can bypass the CORS preflight, do it
        response["preflightheaders"] = Bypass
        response["preflight"] = True
        return response

    # Don't do anything
    return None
#}}}


@logexception(globalInfo, "start")
def start(): # {{{
    urllib3.disable_warnings() # we "know" what we're doing
    global originManifestStore, originManifestApplier, globalInfo
    global globalInfo
    try:
        globalInfo = json.load(open("{}-globalInfo.json".format(statefileprefix)))
    except:
        pass
    originManifestStore = OriginManifestStore(manifest_directory)
    originManifestHeaderMerger = OriginManifestHeaderMerger()
    originManifestApplier = OriginManifestApplier(originManifestHeaderMerger, logfn=ctx.log.debug)

    global urlRouter
    urlRouter = URLRouter([
        # preflight stuff
        ("OPTIONS", "*", "*", "*", '/{any}', cors_preflight),

        # proxy special routes
        ("GET", "*", proxyname, "*", '/', handle_index),
        ("GET", "*", proxyname, "*", '/inspect', handle_inspect),
        ("GET", "*", proxyname, "*", '/inspect/{origin:any}', handle_inspect),
        ("GET", "*", proxyname, "*", '/reset', handle_reset),
        ("GET", "*", proxyname, "*", '/exit', handle_exit),
        ("GET", "*", proxyname, "*", '/info', handle_info),
        ("GET", "*", proxyname, "*", '/getstate', handle_getstate),
        ("GET", "*", proxyname, "*", '/favicon.ico', ignore(200)),
        ("GET", "*", proxyname, "*", '/{any}', ignore(404)),
    ])
    globalInfo["status"] = "started"
#}}}
@logexception(globalInfo, "done")
def done(): #{{{
    global globalInfo
    json.dump(globalInfo, open("{}-globalInfo.json".format(statefileprefix), "w"))
#}}}


