#!/usr/bin/env python3

import sys, pprint, json, time, random
from OriginManifestTools import OriginManifestHeaderMerger

globalReps = 1000000
maxRuntime = 60
maxErrors = 1

def readTestData(fn):
    return json.load(open(fn))

def debug(m):
    print(m)
    pass

def warn(m):
    print(m)
    pass

def check(rep, h1, v1, h2, v2):
    validheader = False
    try:
        merger._doMergeCheck((h1,v1), (h2,v2))
        validheader = True
    except:
        pass

    if validheader:
        for op in ["weaker", "stricter"]:
            ao = h1.lower() == "set-cookie"

            try:
                res = merger.handleOperation((h1,v1), (h2,v2), op, augmentonly=ao)
            except Exception as e:
                warn("Merging ({}/{}): ".format(rep, globalReps))
                warn("  {}: {}".format(h1, v1))
                warn("  {}: {}".format(h2, v2))
                raise e


def runtests(merger, fn):
    testdata = readTestData(fn)
    startTime = time.time()
    errorCount = 0
    for rep in range(globalReps):
        a = random.choice(testdata)
        b = random.choice(testdata)

        h1 = a[0]
        v1 = a[1]

        h2 = b[0]
        v2 = b[1]

        try:
            check(rep, h1, v1, h2, v2)
            check(rep, h1, "", h2, v2)
            check(rep, h1, v1, h2, "")
        except Exception as e:
            warn("EXCEPTION: {}".format(e))
            errorCount += 1

        if time.time() - startTime > maxRuntime:
            warn("Been running longer than {}s, completing {} of {} reps".format(maxRuntime, rep, globalReps))
            return

        if errorCount >= maxErrors:
            warn("More errors than maxErrors ({})".format(maxErrors))
            return


if __name__ == "__main__":
    merger = OriginManifestHeaderMerger()
    for fn in sys.argv[1:]:
        debug("Reading dataset {}...".format(fn))
        runtests(merger, fn)

