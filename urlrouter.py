#!/usr/bin/env python3

from wheezy.routing import PathRouter
from urllib.parse import urlparse

def ignore(code):
    def ignore_inner(kwargs, response):
        response["httpcode"] = code
        return response
    return ignore_inner

class URLRouter():
    def __init__(self, routes):
        self.routesdict = {}

        # process the routes, one PathRouter per (method, scheme, hostname, port) tuple
        for route in routes:
            listroute = list(route)
            method = listroute[0]
            scheme = listroute[1]
            hostname = listroute[2]
            port = listroute[3]
            rest = tuple(listroute[4:])

            key = (method, scheme, hostname, port)
            if key not in self.routesdict:
                self.routesdict[key] = PathRouter()
            self.routesdict[key].add_routes([rest])

    def getWheezyPathRouter(self, method, urlparts):
        """
        Determine if the URL with given urlparts (from urlparse) should be passed
        to wheezy for further processing. If it does, then return the correct PathRouter,
        otherwise return None.
        """

        if urlparts.port == None:
            port = {
                    "http": "80",
                    "https": "443",
            }[urlparts.scheme]
        else:
            port = str(urlparts.port)

        # These are the keys we look for in this order. The first match is returned
        easykeys = []
        for m in [method, "*"]:
            for s in [urlparts.scheme, "*"]:
                for h in [urlparts.hostname, "*"]:
                    for p in [port, "*"]:
                        easykeys += [(m, s, h, p)]

        for key in easykeys:
            if key in self.routesdict:
                return self.routesdict[key]
        return None

    # if we can handle it, return a dict with the response
    # otherwise, return None
    def handle(self, method, url, context={}):
        urlparts = urlparse(url)
        pathrouter = self.getWheezyPathRouter(method, urlparts)

        if pathrouter == None:
            return None

        defaultresponse = {
                "httpcode": 666,
                "data": "",
                "contenttype": "text/html"
        }

        handler, kwargs = pathrouter.match(urlparts.path)
        kwargs["_context"] = context
        kwargs["_method"] = method
        kwargs["_url"] = url
        kwargs["_urlparts"] = urlparts

        if handler == None:
            return None

        out = handler(kwargs, defaultresponse)
        return out






