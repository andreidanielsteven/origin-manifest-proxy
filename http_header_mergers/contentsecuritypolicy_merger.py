import functools
import re 
import sys 

from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


# list of valid CSP directives
VALID_DIRECTIVE_NAMES = [
  "base-uri", "block-all-mixed-content", "child-src", "connect-src",
  "default-src", "disown-opener", "font-src", "form-action", "frame-ancestors",
  "frame-src", "img-src", "manifest-src", "media-src", "object-src",
  "plugin-types", "referrer", "reflected-xss", "report-uri", "report-to",
  "require-sri-for", "sandbox", "script-src", "style-src",
  "upgrade-insecure-requests", "worker-src"
];

# All directives in this list are excluded to fall back to default-src. Their
# expected values do not allow falling back
SPECIAL_DIRECTIVES = ["block-all-mixed-content", "plugin-types", "referrer",
  "require-sri-for-script", "sandbox", "upgrade-insecure-requests"];


###############################################################################
# Functions                                                                   #
###############################################################################

class ContentSecurityPolicy:
  def __init__(self, policy=None):
    self.policy = policy if isinstance(policy, dict) else dict(); 

  # check for string type and validity of name
  def isValidDirectiveName(name):
    global VALID_DIRECTIVE_NAMES;
    return isinstance(name, str) and (name.lower() in VALID_DIRECTIVE_NAMES);

  def __str__(self):
    return self.toString();

  def __repr__(self):
    return self.toString();

  def __eq__(self,other):
    return isinstance(other, ContentSecurityPolicy) \
      and self.policy == other.policy;

  # Convert a string to a dictionary representing a CSP
  # It eliminates invalid directives and cleans up directives with respect to 
  # * and 'none'
  def FromString(csp_str):
    if not isinstance(csp_str, str):
      raise TypeError("'FromString' expects a string.");
    csp = ContentSecurityPolicy(dict());
    directives = csp_str.strip().split(";");
    for directive in directives:
      if (len(directive) == 0):
        continue;
      directive = directive.lower().split();
      dir_name = directive.pop(0);
      if not ContentSecurityPolicy.isValidDirectiveName(dir_name):
        continue;
      # if we have more than just the value 'none' we do not want it
      if "'none'" in directive and len(directive) > 1:
        directive = [x for x in directive if x != "'none'"];
      csp.policy[dir_name] = set(directive);
    return csp;

  def toString(self):
    if not isinstance(self.policy, dict):
      return "";
    return "; ".join(" ".join([k, " ".join(v)]) for k,v in self.policy.items());

  def isEmpty(self):
    return not isinstance(self.policy, dict) or len(self.policy.items()) == 0;
      
# end class ContentSecurityPolicy


# Checks if a string is a "URL" under the definition of CSP. That is it checks
# if a string matches the definition as sepcified in
# https://www.w3.org/TR/CSP3/#grammardef-host-source
def isHostSource(host_src_str):
  # I know the |path_part| is a cheap cheat. But we are not enforcing the CSP
  # here and browsers do the safety check themselves. I simply want to know if
  # it pretends to be a URL or not. If it truely annoys you: replace it with
  # https://tools.ietf.org/html/rfc3986#section-3.3 Go ahead and have "fun".
  scheme_part = "[a-zA-Z]([a-zA-Z0-9]|\+|\-|\.)*";
  host_part = "\*|((\*\.)?[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]+)*)";
  port_part = ":([0-9]+|\*)";
  path_part = "(\/.*)*";
  host_source = "({0}:\/\/)?({1})({2})?({3})?".format(scheme_part, host_part, port_part, path_part);
  return isinstance(host_src_str, str) and \
         re.fullmatch(host_source, host_src_str) != None;


# nonces do not make much sense in the context of Origin Manifest. Therefore
# we kick them out.
# 'nonce-*': disables unsafe-inline, can enable any script with any URL or scheme
# 'sha[256|384|512]-*' disables unsafe-inline, can enable any script with any URL or scheme
# 'strict-dynamic' disables whitelist, schemes, unsafe-inline and self,
#                  allows any non-parser inserted script just because
# Alternative: panic and remove script-src / style-src and default-src
#              to make sure we allow everything
def eliminateNonceHashStrictDynamic(directive, dir_set):
  if directive in SPECIAL_DIRECTIVES:
    return dir_set;

  substitution_set = set({"'unsafe-inline'", "*", "data:", "http:", "https:", "ws:", "wss:"});
  nonce_hash_re = re.compile("('nonce-.+')|('sha(256|384|512)-.+')");
  # we add * below so we also remove any URL just for cosmetics 
  nonce_hash_filter = lambda x: not nonce_hash_re.match(x) and not isHostSource(x);

  has_strict_dynamic = False;
  has_nonce_hash = False;

  if "'strict-dynamic'" in dir_set:
    has_strict_dynamic = True;
    dir_set.remove("'strict-dynamic'");
    dir_set = set(filter(nonce_hash_filter, dir_set));
  else:
    filtered = set(filter(lambda x: not nonce_hash_re.match(x), dir_set));
    if filtered != dir_set:
      has_nonce_hash = True;
    dir_set = filtered;

  if directive in ["script-src", "style-src"]:
    # 'self' is ignored in case of strict-dynamic but should be contained in wildcard
    # TODO what about blob:, file: etc.?
    if has_strict_dynamic:
      dir_set = dir_set.union(substitution_set);
      dir_set.add("'unsafe-eval'");
    elif has_nonce_hash:
      dir_set = dir_set.union(substitution_set);

  if len(dir_set) == 0:
    dir_set = {"'none'"};

  return dir_set;


# Combines two CSPs (represented by dict objects) to a single CSP that allows
# everything from both respective CSPs. That is it does not make things more
# restrictive like normal CSP but results in a wider policy.
def mergeToWeakerContentSecurityPolicy(csp1, csp2):
  combined = ContentSecurityPolicy(dict());

  if not isinstance(csp1, ContentSecurityPolicy) \
     or not isinstance(csp2, ContentSecurityPolicy) \
     or len(csp1.policy) == 0 or len(csp2.policy) == 0:
    return MergeResult(combined, Ops.OP_DELETE_MANIFEST_VALUE);

  for directive in set(csp1.policy.keys()).union(csp2.policy.keys()):
    if directive == "report-to" or directive == "report-uri":
      # we do not want to merge reporting since it does not affect any
      # blocking and there is no way to merge it meaningfully.
      continue;

    # None means no definition whatsoever. Otherwise the definition or
    # (if not a special directive) the default-src.
    set1 = None;
    if directive in csp1.policy:
      set1 = set(csp1.policy[directive]);
    elif "default-src" in csp1.policy and directive not in SPECIAL_DIRECTIVES:
      set1 = set(csp1.policy["default-src"]);
    set2 = None;
    if directive in csp2.policy:
      set2 = set(csp2.policy[directive]);
    elif "default-src" in csp2.policy and directive not in SPECIAL_DIRECTIVES:
      set2 = set(csp2.policy["default-src"]);

    # if any of them is non-existent we skip it that is we do not restrict it
    # at all. Remember, we do not necessarily have a default-src in |combined|.
    # In fact we rely on the fact that we only merge default-src here when both
    # policies define it.
    if set1 != None and set2 != None:
      set1 = eliminateNonceHashStrictDynamic(directive, set1);
      set2 = eliminateNonceHashStrictDynamic(directive, set2);

      # 'none' makes our lives harder... Note that we do not do that with * 
      # because it does not include schemes like data:
      if len(set2) == 1 and "'none'" in set2:
        combined.policy[directive] = set1;
      elif len(set1) == 1 and "'none'" in set1:
        combined.policy[directive] = set2;
      else:
        combined.policy[directive] = set(set1).union(set2);
      if "*" in combined.policy[directive]:
        combined.policy[directive] = set(filter(lambda x: not isHostSource(x),
                                                combined.policy[directive]));
        combined.policy[directive].add("*");

  if combined.isEmpty():
    return MergeResult(combined, Ops.OP_DELETE_MANIFEST_VALUE);
  return MergeResult(combined, Ops.OP_USE_AS_MANIFEST_VALUE);


nonce_hash_strict_dynamic_re = re.compile("('nonce-.+')|('sha(256|384|512)-.+')|'strict-dynamic'");
keywords_filter = lambda x: nonce_hash_strict_dynamic_re.match(x);
def containsNonceHashStrictDynamic(directive):
  for x in directive:
    if keywords_filter(x):
      return True;
  return False;


def mergeToStricterContentSecurityPolicy(csp1, csp2):
  combined = ContentSecurityPolicy(dict());

  if not isinstance(csp1, ContentSecurityPolicy) \
     and isinstance(csp2, ContentSecurityPolicy):
    return MergeResult(csp2, Ops.OP_USE_IN_RESPONSE_HEADER);
  elif isinstance(csp1, ContentSecurityPolicy) \
     and not isinstance(csp2, ContentSecurityPolicy):
    return MergeResult(csp1, Ops.OP_USE_IN_RESPONSE_HEADER);
  elif not isinstance(csp1, ContentSecurityPolicy) \
     and not isinstance(csp2, ContentSecurityPolicy):
    return MergeResult(combined, Ops.OP_DELETE_RESPONSE_HEADER);

  # default-src 'nonce-FOO' is transformed to 'none' but since the nonce
  # affects script-src and style-src we need to copy it over if not defined yet
  # before the transformation.
  if "default-src" in csp1.policy and containsNonceHashStrictDynamic(csp1.policy["default-src"]):
    for directive in ["script-src","style-src"]:
      if directive not in csp1.policy:
        csp1.policy[directive] = set(csp1.policy["default-src"]);
  if "default-src" in csp2.policy and containsNonceHashStrictDynamic(csp2.policy["default-src"]):
    for directive in ["script-src","style-src"]:
      if directive not in csp2.policy:
        csp2.policy[directive] = set(csp2.policy["default-src"]);

  for directive in set(csp1.policy.keys()).union(csp2.policy.keys()):
    if directive == "report-to" or directive == "report-uri":
      # we do not want to merge reporting since it does not affect any
      # blocking and there is no way to merge it meaningfully.
      continue;

    # None means no definition whatsoever. Otherwise the definition or
    # (if not a special directive) the default-src.
    set1 = None;
    if directive in csp1.policy:
      set1 = set(csp1.policy[directive]);
    elif "default-src" in csp1.policy and directive not in SPECIAL_DIRECTIVES:
      set1 = set(csp1.policy["default-src"]);
    set2 = None;
    if directive in csp2.policy:
      set2 = set(csp2.policy[directive]);
    elif "default-src" in csp2.policy and directive not in SPECIAL_DIRECTIVES:
      set2 = set(csp2.policy["default-src"]);

    # If both do not have it there is nothing to do. Otherwise the existing one
    # wins or they have to be merged, respectively.
    if set1 == None and set2 != None:
      combined.policy[directive] = eliminateNonceHashStrictDynamic(directive, set2);
    elif set1 != None and set2 == None:
      combined.policy[directive] = eliminateNonceHashStrictDynamic(directive, set1);
    elif set1 != None and set2 != None:
      # 'none' makes our lives harder... Note that we do not do that with * 
      # because it does not include schemes like data:
      if len(set1) == 1 and "'none'" in set1 \
         or len(set2) == 1 and "'none'" in set2:
        combined.policy[directive] = {"'none'"};
      else:
        set1 = eliminateNonceHashStrictDynamic(directive, set1);
        set2 = eliminateNonceHashStrictDynamic(directive, set2);
        combined.policy[directive] = set1.intersection(set2);
        if "*" in set1 and "*" not in set2:
          combined.policy[directive] = combined.policy[directive].union(
                                         [x for x in set2 if isHostSource(x)]);
        elif "*" not in set1 and "*" in set2:
          combined.policy[directive] = combined.policy[directive].union(
                                         [x for x in set1 if isHostSource(x)]);
        else:
          hostSources1 = [u for u in set1 if isHostSource(u) and u != "*"];
          schemes1 = [s for s in set1 if isSchemeSource(s)];
          hostSources2 = [u for u in set2 if isHostSource(u) and u != "*"];
          schemes2 = [s for s in set2 if isSchemeSource(s)];
          for url in hostSources1:
            combined.policy[directive] = combined.policy[directive].union(
                                             isAllowedByWhitelist(url, hostSources2, schemes2));
          for url in hostSources2:
            combined.policy[directive] = combined.policy[directive].union(
                                             isAllowedByWhitelist(url, hostSources1, schemes1));
    # strictly spoken an empty directive is equal to setting 'none' but
    # being explicit is kinda cooler and you never know which evil non
    # standard conformance exists. But we do not always want to do that.
    if directive not in SPECIAL_DIRECTIVES and len(combined.policy[directive]) == 0:
      combined.policy[directive] = {"'none'"};

  if combined.isEmpty():
    return MergeResult(combined, Ops.OP_DELETE_RESPONSE_HEADER);
  return MergeResult(combined, Ops.OP_USE_IN_RESPONSE_HEADER);


def isSchemeSource(scheme):
  return re.match("^[a-zA-Z]([a-zA-Z0-9]|\+|\-|\.)*:$",str(scheme)) != None;


def hasScheme(url):
  # I include the // here even though they are not part of the scheme.
  # The reason is that I do not want to accept invalid URLs in general.
  return re.match("^[a-zA-Z]([a-zA-Z0-9]|\+|\-|\.)*://",str(url)) != None;


def scheme(url):
  result = re.match("^([a-zA-Z]([a-zA-Z0-9]|\+|\-|\.)*:)//",str(url));
  if result != None and len(result.groups()) >= 1:
    return result.group(1);
  return None;


# Note that we do not actually verify if it is a valid URL without a scheme.
# We just cut the scheme off
def stripScheme(url):
  result = re.match("^[a-zA-Z]([a-zA-Z0-9]|\+|\-|\.)*://(.*)",str(url));
  if result != None and len(result.groups()) == 2:
    return result.group(2);
  return None;


# TODO verify that the function deals with * correctly
def isAllowedByWhitelist(url, hostSources, schemes):
  network_schemes = ["https:","wss:", "http:","ws:"];
  result = set();

  if not isinstance(url,str) or not isHostSource(url) or url == "*":
    return result;

  url1_has_scheme = hasScheme(url);
  url1_scheme = scheme(url);
  url1_strip_scheme = stripScheme(url);
  for url2 in hostSources:
    # a.com and a.com
    if url == url2:
      result.add(url2);
      continue;
    url2_has_scheme = hasScheme(url2);
    # http://a.com and a.com
    if url1_has_scheme and not url2_has_scheme \
       and url1_scheme in network_schemes \
       and url1_strip_scheme == url2:
      result.add(url);
      continue;
    url2_scheme = scheme(url2);
    url2_strip_scheme = stripScheme(url2);
    # a.com and http://a.com
    if not url1_has_scheme and url2_has_scheme \
       and url == url2_strip_scheme and url2_scheme in network_schemes:
      result.add(url2);
    # https://a.com and http://a.com
    elif url1_has_scheme and url2_has_scheme \
       and url1_strip_scheme == url2_strip_scheme \
       and ((url1_scheme == "http:" and url2_scheme == "http:") \
           or (url1_scheme == "ws:" and url2_scheme == "ws:") \
           or (url1_scheme == "https:" and re.match("^https?:$", url2_scheme)) \
           or (url1_scheme == "wss:" and re.match("^wss?:$", url2_scheme))):
        result.add(url);

  for scheme2 in schemes:
    # http://a.com and http:
    if url1_has_scheme \
      and (url1_scheme == scheme2 \
           or ((url1_scheme == "http:" and scheme2 == "http:") \
              or (url1_scheme == "ws:" and scheme2 == "ws:") \
              or (url1_scheme == "https:" and re.match("^https?:$", scheme2)) \
              or (url1_scheme == "wss:" and re.match("^wss?:$", scheme2)))):
        result.add(url);
    # a.com and http:
    if not url1_has_scheme and scheme2 in network_schemes:
      result.add("{0}//{1}".format(scheme2,url));

  return result;


def mergeToWeaker(csp1_str, csp2_str):
  if not isinstance(csp1_str, str) or not isinstance(csp2_str, str):
    raise TypeError("'mergeToWeaker' expects strings only.");

  policies = [x.strip() for x in csp1_str.split(",") if x.strip() != ""] \
              + [x.strip() for x in csp2_str.split(",") if x.strip() != ""];
  policies = list(map(ContentSecurityPolicy.FromString, policies));

  if len(policies) == 0:
    return MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE);
  if len(policies) == 1:
    return MergeResult(policies[0].toString(), Ops.OP_USE_AS_MANIFEST_VALUE);

  result = mergeToWeakerContentSecurityPolicy(policies.pop(), policies.pop());
  while len(policies) != 0:
    result = mergeToWeakerContentSecurityPolicy(result.value, policies.pop());

  if isinstance(result.value, ContentSecurityPolicy):
    result.value = str(result.value);
  return result;


def mergeToStricter(csp1_str, csp2_str):
  if not isinstance(csp1_str, str) or not isinstance(csp2_str, str):
    raise TypeError("'mergeToStricter' expects strings only.");

  policies = [x.strip() for x in csp1_str.split(",") if x.strip() != ""] \
              + [x.strip() for x in csp2_str.split(",") if x.strip() != ""];
  policies = list(map(ContentSecurityPolicy.FromString, policies));

  if len(policies) == 0:
    return MergeResult("", Ops.OP_DELETE_RESPONSE_HEADER);
  if len(policies) == 1:
    return MergeResult(policies[0].toString(), Ops.OP_USE_IN_RESPONSE_HEADER);

  result = mergeToStricterContentSecurityPolicy(policies.pop(), policies.pop());
  while len(policies) != 0:
    result = mergeToWeakerContentSecurityPolicy(result.value, policies.pop());

  if isinstance(result.value, ContentSecurityPolicy):
    result.value = str(result.value);
  return result;


def applyToHeader(policy, header):
  return mergeToStricter(policy, header);
