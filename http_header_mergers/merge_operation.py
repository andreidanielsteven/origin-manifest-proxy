from enum import Enum

class Ops(Enum):
  OP_NOOP                         = 0

  OP_USE_AS_MANIFEST_VALUE        = 10
  OP_DELETE_MANIFEST_VALUE        = 11
  OP_SET_EMPTY_MANIFEST_VALUE     = 12

  OP_USE_IN_RESPONSE_HEADER       = 20
  OP_DELETE_RESPONSE_HEADER       = 21
  OP_SET_EMPTY_RESPONSE_HEADER    = 22

  OP_SEND_CORS_PREFLIGHT          = 30

  OP_FALLBACK_TO_CORS_RESPONSE    = 40
