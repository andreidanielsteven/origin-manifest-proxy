import functools
import re 
import sys 

from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();

VALID_VALUES = ["SAMEORIGIN", "DENY"];

###############################################################################
# Functions                                                                   #
###############################################################################

class XFrameOptions:
  # X-Frame-Options: DENY
  # X-Frame-Options: SAMEORIGIN
  def __init__(self, value=None):
    self.value = value if isinstance(value,str) and value.upper() in VALID_VALUES \
                       else None;

  def __str__(self):
    return self.toString();

  def __repr__(self):
    return self.toString();

  def __eq__(self,other):
    return isinstance(other, XFrameOptions) \
      and self.value == other.value;


  def FromString(input_str):
    if not isinstance(input_str, str):
      raise TypeError("'FromString' expects a string.");
    result = XFrameOptions();
    input_str = input_str.strip();
    if re.match("^DENY$", input_str, re.I):
      result.value = "DENY";
    elif re.match("^SAMEORIGIN$", input_str, re.I):
      result.value = "SAMEORIGIN";
    return result;


  def toString(self):
    return "{0}".format(self.value if self.value != None else "");

# end class XFrameOptions


def mergeToWeakerXFrameOptions(xfo1, xfo2):
  if not isinstance(xfo1, XFrameOptions) or not isinstance(xfo2, XFrameOptions):
    return MergeResult(XFrameOptions(), Ops.OP_DELETE_MANIFEST_VALUE,
                       "Tried to merge with non-XFrameOptions object");

  result = XFrameOptions();
  if xfo1.value == None or xfo2.value == None:
    return MergeResult(result, Ops.OP_DELETE_MANIFEST_VALUE,
                       "XFrameOptions values should never be 'None'.")
  elif xfo1.value == "SAMEORIGIN" or xfo2.value == "SAMEORIGIN":
    result.value = "SAMEORIGIN";
  elif xfo1.value == "DENY" and xfo2.value == "DENY":
    result.value = "DENY"
  return MergeResult(result, Ops.OP_USE_AS_MANIFEST_VALUE, "");


def mergeToStricterXFrameOptions(xfo1, xfo2):
  if not isinstance(xfo1, XFrameOptions) and isinstance(xfo2, XFrameOptions):
    return MergeResult(xfo2, Ops.OP_USE_IN_RESPONSE_HEADER,
                       "Tried to merge with non-XFrameOptions object");
  if isinstance(xfo1, XFrameOptions) and not isinstance(xfo2, XFrameOptions):
    return MergeResult(xfo1, Ops.OP_USE_IN_RESPONSE_HEADER,
                       "Tried to merge with non-XFrameOptions object");
  if not isinstance(xfo1, XFrameOptions) and not isinstance(xfo2, XFrameOptions):
    return MergeResult(XFrameOptions(), Ops.OP_DELETE_RESPONSE_HEADER,
                       "Tried to merge with non-XFrameOptions object");

  result = XFrameOptions();
  if xfo1.value == "DENY" or xfo2.value == "DENY":
    result.value = "DENY"
  elif xfo1.value == "SAMEORIGIN" or xfo2.value == "SAMEORIGIN":
    result.value = "SAMEORIGIN";
  elif xfo1.value == None or xfo2.value == None:
    return MergeResult(result, Ops.OP_DELETE_RESPONSE_HEADER, "Both headers empty");

  return MergeResult(result, Ops.OP_USE_IN_RESPONSE_HEADER, "");


def mergeToWeaker(xfo1_str, xfo2_str):
  if not isinstance(xfo1_str, str) or not isinstance(xfo2_str, str):
    raise TypeError("'mergeToWeaker' expects strings only.");

  xfo1_list = xfo1_str.split(",");
  if len(xfo1_list) > 0:
    xfo1_str = xfo1_list[0].strip();
  xfo2_list = xfo2_str.split(",");
  if len(xfo2_list) > 0:
    xfo2_str = xfo2_list[0].strip();
  result = mergeToWeakerXFrameOptions(
                 XFrameOptions.FromString(xfo1_str),
                 XFrameOptions.FromString(xfo2_str));
  result.value = str(result.value);
  return result;


def mergeToStricter(xfo1_str, xfo2_str):
  if not isinstance(xfo1_str, str) or not isinstance(xfo2_str, str):
    raise TypeError("'mergeToStricter' expects strings only.");

  xfo1_list = xfo1_str.split(",");
  if len(xfo1_list) > 0:
    xfo1_str = xfo1_list[0].strip();
  xfo2_list = xfo2_str.split(",");
  if len(xfo2_list) > 0:
    xfo2_str = xfo2_list[0].strip();
  result = mergeToStricterXFrameOptions(
                 XFrameOptions.FromString(xfo1_str),
                 XFrameOptions.FromString(xfo2_str));
  result.value = str(result.value);
  return result;


def applyToHeader(policy, header):
  return mergeToStricter(policy, header);
