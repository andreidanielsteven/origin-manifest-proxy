import sys 
import re

from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();



###############################################################################
# Functions                                                                   #
###############################################################################

# X-Content-Type-Options: nosniff 
class XContentTypeOptions:
  def __init__(self, nosniff=False):
    self.nosniff = nosniff if isinstance(nosniff, bool) else False;

  def __str__(self):
    return self.toString();

  def __repr__(self):
    return self.toString();

  def __eq__(self,other):
    return isinstance(other, XContentTypeOptions) \
      and self.nosniff == other.nosniff;


  def FromString(input_str):
    if not isinstance(input_str, str):
      raise TypeError("'FromString' expects a string.");
    result = XContentTypeOptions();
    if re.match("^nosniff$", input_str.strip(), re.I):
      result.nosniff = True;
    return result;


  def toString(self):
    return "{0}".format("nosniff" if self.nosniff == True else "");

# end class XContentTypeOptions


def mergeToWeakerXContentTypeOptions(xcto1, xcto2):
  result = XContentTypeOptions();
  if isinstance(xcto1, XContentTypeOptions) \
     and isinstance(xcto2, XContentTypeOptions):
    result.nosniff = xcto1.nosniff == True and xcto2.nosniff == True;
  if result.nosniff == True:
    return MergeResult(result, Ops.OP_USE_AS_MANIFEST_VALUE);
  return MergeResult(result, Ops.OP_DELETE_MANIFEST_VALUE);


def mergeToStricterXContentTypeOptions(xcto1, xcto2):
  result = XContentTypeOptions(
             (isinstance(xcto1, XContentTypeOptions) and xcto1.nosniff == True) \
             or (isinstance(xcto2, XContentTypeOptions) and xcto2.nosniff == True));
  if result.nosniff == True:
    return MergeResult(result, Ops.OP_USE_IN_RESPONSE_HEADER);
  return MergeResult(result, Ops.OP_DELETE_RESPONSE_HEADER);


def mergeToWeaker(xcto1_str, xcto2_str):
  if not isinstance(xcto1_str, str) or not isinstance(xcto2_str, str):
    raise TypeError("'mergeToWeaker' expects strings only.");

  xcto1_list = xcto1_str.split(",");
  if len(xcto1_list) > 0:
    xcto1_str = xcto1_list[0].strip();
  xcto2_list = xcto2_str.split(",");
  if len(xcto2_list) > 0:
    xcto2_str = xcto2_list[0].strip();
  result = mergeToWeakerXContentTypeOptions(
               XContentTypeOptions.FromString(xcto1_str),
               XContentTypeOptions.FromString(xcto2_str));
  result.value = str(result.value);
  return result;


def mergeToStricter(xcto1_str, xcto2_str):
  if not isinstance(xcto1_str, str) or not isinstance(xcto2_str, str):
    raise TypeError("'mergeToStricter' expects strings only.");

  xcto1_list = xcto1_str.split(",");
  if len(xcto1_list) > 0:
    xcto1_str = xcto1_list[0].strip();
  xcto2_list = xcto2_str.split(",");
  if len(xcto2_list) > 0:
    xcto2_str = xcto2_list[0].strip();
  result = mergeToStricterXContentTypeOptions(
               XContentTypeOptions.FromString(xcto1_str),
               XContentTypeOptions.FromString(xcto2_str));
  result.value = str(result.value);
  return result;


def applyToHeader(policy, header):
  return mergeToStricter(policy, header);
