import functools
import sys 

from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Functions                                                                   #
###############################################################################

class TimingAllowOrigin:
  # Timing-Allow-Origin: *
  # Timing-Allow-Origin: <origin>[, <origin>]*
  def __init__(self, wildcard=False,origins=None):
    self.wildcard = wildcard if wildcard == True else False;
    self.origins = origins if origins != None and isinstance(origins,set) \
                           else set();

  def __str__(self):
    return self.toString();

  def __repr__(self):
    return self.toString();

  def __eq__(self,other):
    return isinstance(other, TimingAllowOrigin) \
      and self.wildcard == other.wildcard \
      and self.origins == other.origins;

  def isValidHeader(self):
    return self.wildcard or len(self.origins) > 0;


  def FromString(input_str):
    if not isinstance(input_str, str):
      raise TypeError("'FromString' expects a string.");
    result = TimingAllowOrigin();
    origins_list = set(o.strip() for o in input_str.split(",") if o.strip() != "");
    if len(origins_list) == 0:
      return result;
    if "*" in origins_list:
      result.wildcard = True;
      origins_list.remove("*");
    result.origins = origins_list;
    return result;


  def toString(self):
    wildcard_str = "*" if self.wildcard else "";
    origins_str =  ", ".join(self.origins);
    return ", ".join([x for x in [wildcard_str, origins_str] if x != ""]);

# end class TimingAllowOrigin


def mergeToWeakerTimingAllowOrigin(tao1, tao2):
  result = TimingAllowOrigin();
  result.wildcard = tao1.wildcard == True or tao2.wildcard == True;
  if result.wildcard == True:
    return MergeResult(result, Ops.OP_USE_AS_MANIFEST_VALUE);

  result.origins = tao1.origins.union(tao2.origins);

  if result.isValidHeader():
    return MergeResult(result, Ops.OP_USE_AS_MANIFEST_VALUE);
  return MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE);


def mergeToStricterTimingAllowOrigin(tao1, tao2):
  result = TimingAllowOrigin();
  result.wildcard = tao1.wildcard == True and tao2.wildcard == True;
  if result.wildcard == True:
    return MergeResult(result, Ops.OP_USE_IN_RESPONSE_HEADER);
  if tao1.wildcard == True:
    result.origins = set(tao2.origins);
  elif tao2.wildcard == True:
    result.origins = set(tao1.origins);
  else:
    result.origins = tao1.origins.intersection(tao2.origins);

  if result.isValidHeader():
    return MergeResult(result, Ops.OP_USE_IN_RESPONSE_HEADER);
  return MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER);


def mergeToWeaker(tao1_str, tao2_str):
  if not isinstance(tao1_str, str) or not isinstance(tao2_str, str):
    raise TypeError("'mergeToWeaker' expects strings only.");

  result = mergeToWeakerTimingAllowOrigin(TimingAllowOrigin.FromString(tao1_str),
                                          TimingAllowOrigin.FromString(tao2_str));
  if isinstance(result.value, TimingAllowOrigin):
    result.value = str(result.value);
  return result;


def mergeToStricter(tao1_str, tao2_str):
  if not isinstance(tao1_str, str) or not isinstance(tao2_str, str):
    raise TypeError("'mergeToStricter' expects strings only.");

  result = mergeToStricterTimingAllowOrigin(TimingAllowOrigin.FromString(tao1_str),
                                            TimingAllowOrigin.FromString(tao2_str));
  if isinstance(result.value, TimingAllowOrigin):
    result.value = str(result.value);
  return result;


def applyToHeader(policy, header):
  return mergeToStricter(policy, header);
