#!/bin/bash

tests=(
  contentsecuritypolicy_merger_test
  cors_merger_test
  cors_preflight_merger_test
  hpkp_merger_test
  hsts_merger_test
  set_cookie_merger_test
  timing_allow_origin_merger_test
  x_content_type_options_merger_test
  x_frame_options_merger_test
  x_xss_protection_merger_test
)

if [ ${PWD##*/} == "http_header_mergers" ]
then
  cd ..
fi
for i in ${tests[@]}; do
  echo "***************************************************"
  echo " ${i}"
  echo "***************************************************"
  python3 -m http_header_mergers.${i}
  echo ""
done
