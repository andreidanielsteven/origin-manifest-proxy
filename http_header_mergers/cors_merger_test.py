import json 
import sys 

from .cors_merger import *
from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Tests                                                                       #
###############################################################################

def test():
  testCORSfromStrings();
  testMergeToWeakerCORS();
  testMergeToStricterCORS();
  testCORStoString();
  testMergeToWeaker();
  testMergeToStricter();


def testCORSfromStrings():
  print("Running 'testCORSfromStrings'...");
  test_cases = [
    { "source": 23, "expected": "'FromStrings' expects a dict of strings." },
    { "source": dict(), "expected": CORS() },
    { "source": { "Not-A-CORS-Header": "foobar" }, "expected": CORS() },
    # Access-Control-Allow-Origin: http://foo.example
    { "source": {"Access-Control-Allow-Origin": "http://foo.example"},
      "expected": CORS("http://foo.example") },
    { "source": {"Access-Control-Allow-Origin": "xyz"}, "expected": CORS("xyz") },
    { "source": {"Access-Control-Allow-Origin": "  asdf   "}, "expected": CORS("asdf") },
    # Access-Control-Allow-Credentials: true
    { "source": {"Access-Control-Allow-Credentials": "true"},
      "expected": CORS(None, True) },
    { "source": {"Access-Control-Allow-Credentials": "TrUe"},
      "expected": CORS(None, False) },
    { "source": {"Access-Control-Allow-Credentials": "   true    "},
      "expected": CORS(None, True) },
    { "source": {"Access-Control-Allow-Credentials": "asdf"},
      "expected": CORS(None, False) },
    { "source": {"Access-Control-Allow-Credentials": ""},
      "expected": CORS(None, False) },
    # Everything together
    { "source": {"Access-Control-Allow-Origin": "https://a.com",
                 "Access-Control-Allow-Credentials": "true"},
      "expected": CORS("https://a.com", True) },
    { "source": {"Access-Control-Allow-Credentials": "true",
                 "Access-Control-Allow-Origin": "https://a.com"},
      "expected": CORS("https://a.com", True) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    cors = None;
    try:
      cors = CORS.FromStrings(test["source"]);
    except Exception as e:
      cors = str(e);
    if cors != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' parsed to '{2}' but '{3}' was expected.".format( \
             index, test["source"], cors, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeakerCORS():
  print("Running 'testMergeToWeakerCORS'...");
  test_cases = [
    { "policy1": CORS(), "policy2": CORS(),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": CORS(),
      "policy2": CORS("https://a.com", False),
      "expected": MergeResult(CORS("https://a.com",False),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORS(1234,False),
      "policy2": CORS("https://a.com",False),
      "expected": MergeResult(CORS("https://a.com",False),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORS("https://a.com",False),
      "policy2": CORS("https://a.com",False),
      "expected": MergeResult(CORS("https://a.com",False),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORS("https://a.com",False,),
      "policy2": CORS("https://b.com",False,),
      "expected": MergeResult(CORS("*",False,),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORS("*",False),
      "policy2": CORS("https://b.com",False),
      "expected": MergeResult(CORS("*",False),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORS("*",False),
      "policy2": CORS("https://b.com",True),
      "expected": MergeResult(CORS("*",False),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORS("*",True),
      "policy2": CORS("https://b.com",False),
      "expected": MergeResult(CORS("https://b.com",False),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORS("*",False),
      "policy2": CORS("*",False),
      "expected": MergeResult(CORS("*",False),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORS("https://a.com",None),
      "policy2": CORS("https://a.com",True),
      "expected": MergeResult(CORS("https://a.com",True),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORS("https://a.com",False),
      "policy2": CORS("https://a.com",True),
      "expected": MergeResult(CORS("https://a.com",True),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORS("https://a.com",True),
      "policy2": CORS("https://a.com",True),
      "expected": MergeResult(CORS("https://a.com",True),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    cors1 = mergeToWeakerCORS(test["policy1"],test["policy2"]);
    cors2 = mergeToWeakerCORS(test["policy2"],test["policy1"]);
    if cors1 != test["expected"] or cors2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], cors1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricterCORS():
  print("Running 'testMergeToStricterCORS'...");
  test_cases = [
    { "policy1": CORS(), "policy2": CORS(),
      "expected": MergeResult(None, Ops.OP_FALLBACK_TO_CORS_RESPONSE) },
    { "policy1": CORS(),
      "policy2": CORS("https://a.com",False),
      "expected": MergeResult(None, Ops.OP_FALLBACK_TO_CORS_RESPONSE) },
    { "policy1": CORS(1234,False),
      "policy2": CORS("https://a.com",False),
      "expected": MergeResult(None, Ops.OP_FALLBACK_TO_CORS_RESPONSE) },
    { "policy1": CORS("https://a.com",False),
      "policy2": CORS("https://a.com",False),
      "expected": MergeResult(CORS("https://a.com",False),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORS("https://a.com",False),
      "policy2": CORS("https://b.com",False),
      "expected": MergeResult(None, Ops.OP_FALLBACK_TO_CORS_RESPONSE) },
    { "policy1": CORS("*",False),
      "policy2": CORS("https://b.com",False),
      "expected": MergeResult(CORS("https://b.com",False),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORS("*",False),
      "policy2": CORS("https://b.com",True),
      "expected": MergeResult(CORS("https://b.com",False),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORS("*",True),
      "policy2": CORS("https://b.com",False),
      "expected": MergeResult(None, Ops.OP_FALLBACK_TO_CORS_RESPONSE) },
    { "policy1": CORS("*",False),
      "policy2": CORS("*",False),
      "expected": MergeResult(CORS("*",False),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORS("https://a.com",None),
      "policy2": CORS("https://a.com",True),
      "expected": MergeResult(CORS("https://a.com",False),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORS("https://a.com",False),
      "policy2": CORS("https://a.com",True),
      "expected": MergeResult(CORS("https://a.com",False),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORS("https://a.com",True),
      "policy2": CORS("https://a.com",True),
      "expected": MergeResult(CORS("https://a.com",True),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    cors1 = mergeToStricterCORS(test["policy1"],test["policy2"]);
    cors2 = mergeToStricterCORS(test["policy2"],test["policy1"]);
    if cors1 != test["expected"] or cors2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], cors1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testCORStoString():
  print("Running 'testCORStoString'...");
  test_cases = [
    { "source": CORS(), "expected": ["{}"] },
    { "source": CORS("https://b.com",True),
      "expected": [json.dumps({"Access-Control-Allow-Origin": "https://b.com",
                               "Access-Control-Allow-Credentials": "true"})] },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output_str = test["source"].toString();
    if output_str not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' transformed to '{2}' but any in '{3}' was "
            "expected.".format(index, test["source"], output_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeaker():
  print("Running 'testMergeToWeaker'...");
  print("TODO");
  test_cases = [
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output1_str = mergeToWeaker(test["policy1"],test["policy2"]);
    output2_str = mergeToWeaker(test["policy1"],test["policy2"]);
    if output1_str not in test["expected"] or output2_str not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], output1_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricter():
  print("Running 'testMergeToStricter'...");
  print("TODO");
  test_cases = [
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output1_str = mergeToStricter(test["policy1"],test["policy2"]);
    output2_str = mergeToStricter(test["policy1"],test["policy2"]);
    if output1_str not in test["expected"] or output2_str not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], output1_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


###############################################################################
# Main                                                                        #
###############################################################################

if __name__ == "__main__":
  test();
