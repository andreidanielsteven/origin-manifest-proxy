import sys 
import re

from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();



###############################################################################
# Functions                                                                   #
###############################################################################

class SetCookie:
  # Set-Cookie: <cookie-name>=<cookie-value>; Secure
  # Set-Cookie: <cookie-name>=<cookie-value>; HttpOnly
  def __init__(self, secure=False, httponly=False):
    self.secure = secure;
    self.httponly = httponly;

  def __str__(self):
    return self.toString();

  def __repr__(self):
    return self.toString();

  def __eq__(self,other):
    return isinstance(other, SetCookie) \
      and self.secure == other.secure \
      and self.httponly == other.httponly;

  def FromString(input_str):
    if not isinstance(input_str, str):
      raise TypeError("'FromString' expects a string.");
    result = SetCookie();
    for parts in input_str.split(";"):
      attribute = parts.strip();
      if re.match("^secure$", attribute, re.I):
        result.secure = True;
      if re.match("^httpOnly$", attribute, re.I):
        result.httponly = True;
    return result;


  def toString(self):
    result = ["secure" if self.secure else "",
              "httpOnly" if self.httponly else ""];
    return "; ".join(x for x in result if x != "");

# end class SetCookie


def mergeToWeakerSetCookie(sc1, sc2):
  result = SetCookie();
  result.secure = sc1.secure == True and sc2.secure == True;
  result.httponly = sc1.httponly == True and sc2.httponly == True;

  if result.secure != True and result.httponly != True:
    return MergeResult(result, Ops.OP_DELETE_MANIFEST_VALUE);
  return MergeResult(result, Ops.OP_USE_AS_MANIFEST_VALUE);


def mergeToStricterSetCookie(sc1, sc2):
  result = SetCookie();
  result.secure = sc1.secure == True or sc2.secure == True;
  result.httponly = sc1.httponly == True or sc2.httponly == True;

  if result.secure != True and result.httponly != True:
    return MergeResult(result, Ops.OP_NOOP);
  return MergeResult(result, Ops.OP_USE_IN_RESPONSE_HEADER);


def mergeToWeaker(sc1_str, sc2_str):
  if not isinstance(sc1_str, str) or not isinstance(sc2_str, str):
    raise TypeError("'mergeToWeaker' expects strings only.");

  result = mergeToWeakerSetCookie(SetCookie.FromString(sc1_str),
                                  SetCookie.FromString(sc2_str));
  result.value = str(result.value);
  return result;


def mergeToStricter(sc1_str, sc2_str):
  if not isinstance(sc1_str, str) or not isinstance(sc2_str, str):
    raise TypeError("'mergeToStricter' expects strings only.");

  result = mergeToStricterSetCookie(SetCookie.FromString(sc1_str),
                                    SetCookie.FromString(sc2_str));
  result.value = str(result.value);
  return result;


def applyToHeader(policy, header):
  header_sc = SetCookie.FromString(header);
  stricter = mergeToStricterSetCookie(SetCookie.FromString(policy), header_sc);
  if stricter.operation != Ops.OP_USE_IN_RESPONSE_HEADER:
    return MergeResult(header, Ops.OP_NOOP);
  if not header_sc.secure and stricter.value.secure:
    header = "{0}; secure".format(header);
  if not header_sc.httponly and stricter.value.httponly:
    header = "{0}; httpOnly".format(header);
  return MergeResult(header, Ops.OP_USE_IN_RESPONSE_HEADER);
  
