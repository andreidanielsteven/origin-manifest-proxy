from mitmproxy import http, ctx
from urlrouter import URLRouter, ignore
from commontools import *
import sys, json, os, pprint

# Usage: ./origin-manifest-networkproxy.py <proxyname> 

proxyname = sys.argv[1]
statefileprefix = sys.argv[2]

####################################################################
### only code below this point
####################################################################

globalInfo = {"status": "prestart"}

requestResponseList = []

@logexception(globalInfo,"request")
def request(flow: http.HTTPFlow) -> None: #{{{
    global urlRouter

    handlercontext = {
        "flow": flow,
        "ctx": ctx,
    }

    result = urlRouter.handle(flow.request.method.upper(),
                              flow.request.pretty_url,
                              context=handlercontext)
    if result != None:
        if result["httpcode"] == 302:
            # special case: 302 redirect
            flow.response = http.HTTPResponse.make(result["httpcode"],
                                                   result["data"], 
                                                   {"location": result["location"]})
        else:
            flow.response = http.HTTPResponse.make(result["httpcode"],
                                                   result["data"], 
                                                   {"content-type": result["contenttype"]})
        return
#}}}

@logexception(globalInfo, "response")
def response(flow: http.HTTPFlow) -> None: #{{{
    if flow.request.host == proxyname:
      return

    global requestResponseList
    requestResponseList.append({
      "url": flow.request.url,
      "origin": getRequestOrigin(flow),
      "method": flow.request.method,
      "request_headers": [[h[0],h[1]] for h in flow.request.headers.items()],
      "response_headers": [[h[0],h[1]] for h in flow.response.headers.items()]
    })
#}}}

def handle_index(args, response): #{{{
    response["data"] = """
<html><body>
<h1>Server proxy</h1>
<dl>
<dt><a href="/inspect">Inspect</a></dt>
<dd>Inspect collected request and response headers.</dd>

<dt><a href="/dump">Dump</a></dt>
<dd>Write response and request headers to disk.</dd>

<dt><a href="/reset">Reset</a></dt>
<dd>Reset proxy state by forgetting all collected request and response headers</dd>

<dt><a href="/info">Info</a></dt>
<dd>Return some internal proxy information</dd>

<dt><a href="/exit">Exit proxy</a></dt>
<dd>... for a clean shutdown</dd>
</dl>
</body></html>
"""
    response["httpcode"] = 200
    return response
#}}}

def handle_inspect(args, response): #{{{
    response["data"] = """
<html><body>
<a href="/">back</a>
<p>{0}</p>
</body></html>
""".format(json.dumps(requestResponseList))
    response["httpcode"] = 200
    return response
#}}}

def handle_getstate(args, response): # {{{
    global requestResponseList
    response["data"] = json.dumps(requestResponseList)
    response["httpcode"] = 200
    response["contenttype"] = "application/json"
    return response
#}}}

def handle_reset(args, response): #{{{
    global requestResponseList
    requestResponseList = []

    html  = "<html><body>"
    html += "<a href=\"/\">back</a>"
    html += "<p>Requests and responses forgotten.</p>"
    html += "</body></html>"

    response["data"] = html
    response["httpcode"] = 200
    return response
#}}}

def handle_info(args, response): # {{{
    global globalInfo
    response["data"] = json.dumps(globalInfo)
    response["httpcode"] = 200
    response["contenttype"] = "application/json"
    return response
#}}}

def handle_exit(args, response): # {{{
    ctx = args["_context"]["ctx"]
    ctx.log.warn("Exit on demand")
    ctx.master.should_exit.set()
    return response
#}}}

@logexception(globalInfo, "start")
def start(): #{{{
    global urlRouter, globalInfo, requestResponseList
    try:
        globalInfo = json.load(open("{}-globalInfo.json".format(statefileprefix)))
    except:
        pass
    try:
        requestResponseList = json.load(open("{}-requestResponseList.json".format(statefileprefix)))
    except:
        pass

    urlRouter = URLRouter([
        # proxy special routes
        ("GET", "*", proxyname, "*", '/', handle_index),
        ("GET", "*", proxyname, "*", '/inspect', handle_inspect),
        ("GET", "*", proxyname, "*", '/getstate', handle_getstate),
        ("GET", "*", proxyname, "*", '/reset', handle_reset),
        ("GET", "*", proxyname, "*", '/info', handle_info),
        ("GET", "*", proxyname, "*", '/exit', handle_exit),
        ("GET", "*", proxyname, "*", '/favicon.ico', ignore(200)),
        ("GET", "*", proxyname, "*", '/{any}', ignore(404)),
    ])
    globalInfo["status"] = "started"
#}}}

@logexception(globalInfo, "done")
def done(): #{{{
    global globalInfo, requestResponseList
    json.dump(globalInfo, open("{}-globalInfo.json".format(statefileprefix), "w"))    
    json.dump(requestResponseList, open("{}-requestResponseList.json".format(statefileprefix), "w"))    
    print("done")
#}}}
