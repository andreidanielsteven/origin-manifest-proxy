#!/usr/bin/env python3

# load some requests files
# without origin: print out a list of origins with number of requests
# with origin: print out the manifest

import sys, json, bz2, pprint
from OriginManifestTools import *

def myopen(fn):
    fp = None
    try:
        fp = bz2.open(fn, "rt")
    except Exception as e:
        fp = open(fn)
    return fp


OMlearner = OriginManifestLearner()
OMmerger = OriginManifestHeaderMerger()
OMgenerator = OriginManifestGenerator(OMlearner, OMmerger, cutoff=60, minsize=2, blacklist=[])

for fn in sys.argv[1:]:
    data = json.load(myopen(fn))
    for rec in data:
        headerdict = dict([(x[0], x[1]) for x in rec["headers"]])
        OMlearner.learn(rec["method"], rec["url"], headerdict)
OMlearner.writeState("out.json")

