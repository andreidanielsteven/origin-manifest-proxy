containernameprefix := origin-manifest-proxy

#servertool := /bin/bash
servertool := mitmdump -s "/data/origin-manifest-serverproxy.py /data"
serverdockeropts := --hostname $(containernameprefix)-server  --rm -e LANG=en_US.UTF-8 -v $$PWD:/data

#clienttool := /bin/bash
clienttool := mitmdump --insecure -U http://$(containernameprefix)-server:8080 -s "/data/origin-manifest-clientproxy.py /data"
clientdockeropts := --hostname $(containernameprefix)-client -p 8080:8080 --link $(containernameprefix)-server --rm -e LANG=en_US.UTF-8 -v $$PWD:/data

fullbuild:
	@docker build --no-cache -t $(containernameprefix)-common -f Dockerfile.common .
	@docker build --no-cache -t $(containernameprefix)-client -f Dockerfile.client .
	@docker build --no-cache -t $(containernameprefix)-server -f Dockerfile.server .
	@docker build --no-cache -t $(containernameprefix)-combined -f Dockerfile.combined .

build:
	@docker build -t $(containernameprefix)-common -f Dockerfile.common .
	@docker build -t $(containernameprefix)-client -f Dockerfile.client .
	@docker build -t $(containernameprefix)-server -f Dockerfile.server .
	@docker build -t $(containernameprefix)-combined -f Dockerfile.combined .

clean:
	@docker kill $(containernameprefix)-client $(containernameprefix)-server $(containernameprefix)-common $(containernameprefix)-combined || true
	@docker rm $(containernameprefix)-client $(containernameprefix)-server $(containernameprefix)-common $(containernameprefix)-combined || true
	@rm -f *~

realclean: clean
	@docker rmi $(containernameprefix)-client $(containernameprefix)-server $(containernameprefix)-common $(containernameprefix)-combined || true
	@docker container prune -f 
	@docker image prune -f 

runbg:
	@docker run -d --name $(containernameprefix)-server $(serverdockeropts) $(containernameprefix)-server $(servertool)
	@docker run -d --name $(containernameprefix)-client $(clientdockeropts) $(containernameprefix)-client $(clienttool)

runserver:
	@docker run -ti --name $(containernameprefix)-server $(serverdockeropts) $(containernameprefix)-server $(servertool)

runclient:
	@docker run -ti --name $(containernameprefix)-client $(clientdockeropts) $(containernameprefix)-client $(clienttool)

runcombined:
	@docker run -ti --name $(containernameprefix)-combined $(containernameprefix)-combined
