#!/usr/bin/env python3

from traceback import format_exc

def getRequestOrigin(flow): #{{{
    return "{},{},{}".format(flow.request.scheme, flow.request.host, flow.request.port).lower()
#}}}
def logexception(store, name): #{{{
    def realdecorator(fn):
        def wrapper(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except Exception as e:
                if "exceptions" not in store:
                    store["exceptions"] = {}
                if name not in store["exceptions"]:
                    store["exceptions"][name] = []
                store["exceptions"][name] += [format_exc()]
                raise e
        return wrapper
    return realdecorator
#}}}
