#!/usr/bin/env python3

import sys, pprint, json, time
from OriginManifestTools import OriginManifestHeaderMerger

def readTestData(fn):
    return json.load(open(fn))

def debug(m):
    #print(m)
    pass

def warn(m):
    print(m)
    pass

def runtests(merger, dataset):
    hcount = 0
    snonecount = 0
    wnonecount = 0

    weakertime = 0
    weakercount = 0
    tmptime = 0
    strictertime = 0
    strictercount = 0

    for x in testdata:
        h1 = x[0]
        h2 = x[0]
        v1 = x[1]
        v2 = x[1]

        validheader = False
        try:
            merger._doMergeCheck((h1,v1), (h2,v2))
            validheader = True
        except:
            pass

        if validheader:
            hcount += 1
            debug("Found header  : {}".format(h1))
            debug("Merging Value1: {}".format(v1))
            debug("...with Value2: {}".format(v2))

            tmptime = time.time()
            resw = merger.weaker((h1,v1), (h2,v2))
            # FIXME
            weakertime += (time.time() - tmptime)
            weakercount += 1
            debug("Weaker        : {}".format(resw["value"]))
            tmptime = time.time()
            ress = merger.stricter((h1,v1), (h2,v2))
            strictertime += (time.time() - tmptime)
            strictercount += 1
            debug("Stricter        : {}".format(ress["value"]))

            if ress["value"] == None:
                snonecount += 1
                debug("Should this be None?")

            if resw["value"] == None:
                wnonecount += 1
                debug("Should this be None?")

            # cross-check to see if functions return valid strings
            for a in [v1, v2, resw["value"], ress["value"]]:
                if a == None:
                    continue
                for b in [v1, v2, resw["value"], ress["value"]]:
                    if b == None:
                        continue
                    #print("Merging {} and {}".format(a,b))
                    tmptime = time.time()
                    merger.weaker((h1,a), (h1,b))
                    weakertime += (time.time() - tmptime)
                    weakercount += 1
                    tmptime = time.time()
                    merger.stricter((h1,a), (h1,b))
                    strictertime += (time.time() - tmptime)
                    strictercount += 1

    return (hcount, snonecount, wnonecount, weakertime, weakercount, strictertime, strictercount)

if __name__ == "__main__":
    merger = OriginManifestHeaderMerger()
    for fn in sys.argv[1:]:
        debug("Reading dataset {}...".format(fn))
        testdata = readTestData(fn)
        try:
            (hcount, snonecount, wnonecount, weakertime, weakercount, strictertime, strictercount) = runtests(merger, testdata)
            warn("==> Dataset {}: Processed {} valid headers, with {} None stricter merges, and {} None weaker merges. AVG weaker: {:.2f}ms, stricter: {:.2f}ms".format(fn, hcount, snonecount, wnonecount, 1000.0 * weakertime/weakercount, 1000.0 * strictertime/strictercount))
        except Exception as e:
            warn("==> Dataset {}: EXCEPTION: {}".format(fn, e))

