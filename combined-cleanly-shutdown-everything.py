#!/bin/bash

# This is not a python file, I just got lazy...

httpproxies="post-http-counter serverproxy mid-http-counter clientproxy pre-http-counter"
tcpproxies="pre-tcp-counter mid-tcp-counter post-tcp-counter"

# first, tell all http proxies to dump their data
for proxyname in $httpproxies;
do
    ./fetch-through-proxy.py http://0.0.0.0:8080 http://$proxyname/dump
done

# next, stop all tcp proxies
for supervisorname in $tcpproxies;
do
    supervisorctl stop $supervisorname
done

