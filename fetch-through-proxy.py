#!/usr/bin/env python3

import sys, requests, pprint

def fetchWithProxyREQ(proxyurl, url):
    proxies = {
        "http": proxyurl,
        "https": proxyurl,
    }
    result = requests.get(url, verify=False, proxies=proxies)
    return result.headers, result.text

if __name__ == "__main__":
    h,d = fetchWithProxyREQ(sys.argv[1], sys.argv[2])
    pprint.pprint(h)
    pprint.pprint(d)
