#!/usr/bin/python
# This is a simple port-forward / proxy, written using only the default python
# library. If you want to make a suggestion or fix something you can contact-me
# at voorloop_at_gmail.com
# Distributed over IDC(I Don't Care) license
import socket
import select
import time
import sys
import json
import signal
import errno

# Changing the buffer_size and delay, you can improve the speed and bandwidth.
# But when buffer get to high or delay go too down, you can broke things
buffer_size = 4096
delay = 0.0001

def resetState(*args):
    global bytes_counter
    bytes_counter = {
      # counter for bytes originating from the client
      "sent": 0,
      # counter for bytes originating from the server
      "received": 0
    }

def dumpState(*args):
    global bytes_counter, log_file
    json.dump(bytes_counter, open(log_file, "w"))

def loadState(*args):
    global bytes_counter, log_file
    try:
        bytes_counter = json.load(open(log_file))
    except:
        pass

class Forward:
    def __init__(self):
        self.forward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def start(self, host, port):
        try:
            self.forward.connect((host, port))
            return self.forward
        except Exception, e:
            print e
            return False

class TheServer:
    input_list = []
    channel = {}
    upstreamsockets = set()

    def __init__(self, host, port):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((host, port))
        self.server.listen(200)

    def main_loop(self):
        self.input_list.append(self.server)
        while 1:
            time.sleep(delay)
            ss = select.select
            inputready = []
            try:
                inputready, outputready, exceptready = ss(self.input_list, [], [])
            except select.error, e:
                if e[0] != errno.EINTR:
                    raise

            for self.s in inputready:
                if self.s == self.server:
                    self.on_accept()
                    break

                self.data = ""
                try:
                    self.data = self.s.recv(buffer_size)
                except:
                    pass
                    
                if len(self.data) == 0:
                    self.on_close()
                    break
                else:
                    self.on_recv()

    def on_accept(self):
        forward = Forward().start(forward_to[0], forward_to[1])
        clientsock, clientaddr = self.server.accept()
        if forward:
            self.input_list.append(clientsock)
            self.input_list.append(forward)
            self.channel[clientsock] = forward
            self.channel[forward] = clientsock
            self.upstreamsockets.add(forward)
        else:
            clientsock.close()

    def on_close(self):
        #remove objects from input_list
        self.input_list.remove(self.s)
        self.input_list.remove(self.channel[self.s])
        out = self.channel[self.s]
        # close the connection with client
        self.channel[out].close()  # equivalent to do self.s.close()
        # close the connection with remote server
        self.channel[self.s].close()
        # delete both objects from channel dict
        del self.channel[out]
        del self.channel[self.s]
        # remove from upstreamsockets
        [self.upstreamsockets.discard(x) for x in [out, self.s]]

    def on_recv(self):
        global bytes_counter
        data = self.data
        # here we can parse and/or modify the data before send forward
        self.channel[self.s].send(data)
        # log data
        datadir = "received" if self.s in self.upstreamsockets else "sent"
        datalen = len(data)
        bytes_counter[datadir] += datalen

if __name__ == '__main__':
        localport = int(sys.argv[1])
        forward_to = (sys.argv[2], int(sys.argv[3]))
        log_file = sys.argv[4]
        bytes_counter = None

        resetState()
        loadState()

        signal.signal(signal.SIGUSR1, dumpState)
        signal.signal(signal.SIGUSR2, resetState)

        server = TheServer('', localport)
        try:
            server.main_loop()
        except KeyboardInterrupt:
            sys.exit(1)
